# lemmi-v2

See [https://lemmi.ezlab.org/](https://lemmi.ezlab.org/)
In short:

```bash
git clone https://gitlab.com/ezlab/lemmi-v2.git
cd lemmi-v2
git checkout tags/v2.2.0
export LEMMI_ROOT=$(pwd)
export PATH=${LEMMI_ROOT}/workflow/scripts:$PATH

conda install -n base -c conda-forge mamba
mamba env update -n lemmi --file ${LEMMI_ROOT}/workflow/envs/lemmi.yaml
conda activate lemmi

lemmi_full --cores 8 # running locally on Docker with 8 cpus

# For Apptainer/singularity
cd ${LEMMI_ROOT}/benchmark/sif/
singularity build lemmi_master.sif docker://quay.io/ezlab/lemmi_master:v2.2.0_cv1
singularity build kraken_212.sif docker://quay.io/ezlab/kraken_212_lemmi:v2.2.0_cv1
lemmi_full --cores 8 --use-singularity 
```
