import pandas as pd
import toolbox
import os
import urllib
from urllib.error import HTTPError

root = toolbox.set_root()
tmp = toolbox.set_tmp(root, config)

os.chdir(tmp)

acc = toolbox.define_accessions(root)

# load genebank assembly
gbas = pd.read_csv(
    "{}repository/{}".format(
        root, config["genbank_assembly"].split("/")[-1].split("|")[0]
    ),
    sep="\t",
    header=0,
)

toolbox.validate_config(config)


localrules:
    target,


rule target:
    input:
        genomes=expand("{root}repository/genomes/{acc}.fna.gz", root=root, acc=acc),
        broken=expand("{root}repository/genomes/404.txt", root=root),


rule broken:
    output:
        expand("{root}repository/genomes/404.txt", root=root),
    shell:
        """
        touch {output}
        """


rule genomes_download:
    input:
        genbank_assembly=expand(
            "{root}repository/{genbank_assembly}",
            root=root,
            genbank_assembly=config["genbank_assembly"].split("/")[-1].split("|")[0],
        ),
    output:
        genomes=expand("{root}repository/genomes/{{acc}}.fna.gz", root=root),
    message:
        "Downloading a genome..."
    threads: 1
    run:
        try:
            url = gbas[gbas["# assembly_accession"] == wildcards.acc][
                "ftp_path"
            ].values[0]
            name = url.split("/")[-1]
            dlurl = os.path.join(url, "{}_genomic.fna.gz".format(name))
            urllib.request.urlretrieve(dlurl, output.genomes[0])
        except IndexError as e:
            # rethrow as it should not happen
            raise e
        except HTTPError:
            open(output.genomes[0], "w").close()
            broken = open("{}repository/genomes/404.txt".format(root), "a")
            broken.write("{}\n".format(wildcards.acc))
            broken.close()
