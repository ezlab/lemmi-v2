# TODO fuse duplicated code (optimal and fixed threshold metrics)
import glob
import shutil
import os

import numpy as np
import pandas as pd
import toolbox
import yaml

from scipy.spatial import distance

root = toolbox.set_root()
tmp = toolbox.set_tmp(root, config)

os.chdir(tmp)

# check if the ncbi prok list should be used
prok_suffix = ""
use_prokncbi = False
if config["use_prokncbi"] == 1:
    prok_suffix = "ncbi"
    use_prokncbi = True

# note: this nosuffix concept is not used in practice.
# The idea was to enable different evaluation of the same data
# simulating the presence a different instance for that purpose
# should be removed instance_name_nosuffix => instance_name
# {suffix} can be removed entirely, there is room for simplification here
config["instance_name_nosuffix"] = config["instance_name"].split("___")[0]
try:
    suffix = "___" + config["instance_name"].split("___")[1]
except IndexError:
    suffix = ""

# load the yaml definintion of the sample
with open(
    "{}yaml/instances/{}.yaml".format(root, config["instance_name_nosuffix"]), "r"
) as file:
    sample_description = yaml.safe_load(file)
# define what the tool and the samples are
negative_samples_ids = ["n{}".format(s) for s in sample_description["negative"]]
calibration_samples_ids = ["c{}".format(s) for s in sample_description["calibration"]]
evaluation_samples_ids = ["e{}".format(s) for s in sample_description["evaluation"]]
all_samples_ids = (
    negative_samples_ids + calibration_samples_ids + evaluation_samples_ids
)

if "real_samples" not in config:
    config["real_samples"] = []

toolnames = set(
    [
        predictions_path.split("/")[-1].split(".")[0]
        for predictions_path in glob.glob(
            "{}analysis_outputs/*.{}*.predictions.tsv".format(
                root, config["instance_name_nosuffix"]
            )
        )
        if predictions_path.split("/")[-1].split(".")[0] not in config["no_evaluation"]
    ]
)

toolbox.validate_config(config)

FILTERING_STEP = 3


wildcard_constraints:
    all_samples_ids="|".join(all_samples_ids),
    evaluation_samples_ids="|".join(evaluation_samples_ids),
    calibration_samples_ids="|".join(calibration_samples_ids),
    negative_samples_ids="|".join(negative_samples_ids),
    evaluation_sample="|".join(
        f"{config['instance_name_nosuffix']}-{s}" for s in evaluation_samples_ids
    ),
    calibration_sample="|".join(
        f"{config['instance_name_nosuffix']}-{s}" for s in calibration_samples_ids
    ),
    negative_sample="|".join(
        f"{config['instance_name_nosuffix']}-{s}" for s in negative_samples_ids
    ),
    rank="|".join(config["evaluation_taxlevel"]),


localrules:
    target,


rule target:
    input:
        expand(
            "{root}evaluations/predictions.{toolname}.{all_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            all_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
                + calibration_samples_ids
                + negative_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}evaluations/filtering_threshold.{toolname}.{instance_name}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            instance_name=config["instance_name"],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}evaluations/precision.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}evaluations/precision_{threshold}.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            threshold=str(config["filtering_threshold"]).split(".")[-1],
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}evaluations/recall_{threshold}.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            threshold=str(config["filtering_threshold"]).split(".")[-1],
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}evaluations/f1_{threshold}.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            threshold=str(config["filtering_threshold"]).split(".")[-1],
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}final_results/{instance_name}.precision.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.precision.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.precision_{threshold}.tsv",
            instance_name=config["instance_name"],
            threshold=str(config["filtering_threshold"]).split(".")[-1],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.recall_{threshold}.tsv",
            instance_name=config["instance_name"],
            threshold=str(config["filtering_threshold"]).split(".")[-1],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.f1_{threshold}.tsv",
            instance_name=config["instance_name"],
            threshold=str(config["filtering_threshold"]).split(".")[-1],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.precision_{threshold}.json",
            instance_name=config["instance_name"],
            threshold=str(config["filtering_threshold"]).split(".")[-1],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.recall_{threshold}.json",
            instance_name=config["instance_name"],
            threshold=str(config["filtering_threshold"]).split(".")[-1],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.f1_{threshold}.json",
            instance_name=config["instance_name"],
            threshold=str(config["filtering_threshold"]).split(".")[-1],
            root=root,
        ),
        expand(
            "{root}evaluations/recall.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}final_results/{instance_name}.recall.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.recall.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}evaluations/tp.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}final_results/{instance_name}.tp.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.tp.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}evaluations/fp.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}final_results/{instance_name}.fp.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.fp.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}evaluations/l2.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}final_results/{instance_name}.l2.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.l2.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}evaluations/f1.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.f1.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_analysis.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_analysis.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_analysis.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_analysis.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_host.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_host.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_host.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_host.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_contaminants.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_contaminants.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_contaminants.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_contaminants.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_targets.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_targets.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_targets.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_targets.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.filtering_threshold.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.filtering_threshold.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.auprc.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.auprc.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.prc.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.prc.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{toolname}.{all_sample}.{rank}.tsv",
            toolname=toolnames,
            all_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
                + calibration_samples_ids
                + negative_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
            root=root,
        ),
        expand(
            "{root}final_results/{evaluation_samples_ids}.json",
            toolname=toolnames,
            evaluation_samples_ids=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.structure.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.textbox.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.ranks.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}evaluations/{evaluation_sample}.pairwise.{toolnamesA}.{toolnamesB}.{rank}.txt",
            root=root,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            toolnamesA=toolnames,
            toolnamesB=toolnames,
            rank=config["evaluation_taxlevel"],
        ),
        expand(
            "{root}final_results/{evaluation_sample}.pairwise.json",
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            root=root,
        ),


rule predictions_by_ranks:
    input:
        predictions=expand(
            "{root}analysis_outputs/{{toolname}}.{{instance_name_nosuffix}}-{{all_sample_ids}}.predictions.tsv",
            root=root,
        ),
        ref_genomes=expand(
            "{root}instances/{instance_name}/{instance_name}.{target}.all_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
            target=config["targets"],
        ),
    output:
        expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{all_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
    message:
        "Sum the predictions at the given rank"
    threads: 1
    run:
        toolbox.sum_to_taxlevel(
            input.predictions[0],
            input.ref_genomes,
            config["targets_taxonomy"],
            wildcards.rank,
            config,
            output[0],
        )


rule define_thresholds:
    input:
        predictions_file=expand(
            "{root}evaluations/predictions.{{toolname}}.{calibration_sample}.{{rank}}.tsv",
            root=root,
            calibration_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in calibration_samples_ids
            ],
        ),
        truth=expand(
            "{root}yaml/instances/samples/{calibration_sample}.yaml",
            root=root,
            calibration_sample=[
                "{}-{}".format(config["instance_name_nosuffix"], s)
                for s in calibration_samples_ids
            ],
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
    output:
        filtering_threshold=expand(
            "{root}evaluations/filtering_threshold.{{toolname}}.{{instance_name}}.{{rank}}.tsv",
            root=root,
        ),
        auprc=expand(
            "{root}evaluations/auprc.{{toolname}}.{{instance_name}}.{{rank}}.tsv",
            root=root,
        ),
        prc=expand(
            "{root}evaluations/prc.{{toolname}}.{{instance_name}}.{{rank}}.tsv",
            root=root,
        ),
    message:
        "Exploring the AUprC and defining the threshold to stay at the FDR..."
    threads: 1
    run:
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])

        thresholds = []
        pr = []
        for i, file in enumerate(input.predictions_file):
            truth = toolbox.load_truth(
                input.truth[i],
                reference_lists,
                config,
                wildcards.rank,
                input.unknown_lineages[0],
            )

            predictions = pd.read_csv(file, header=1, sep="\t")
            threshold = 0
            FDR_threshold = -1
            best_f1 = 0
            first_pass = True
            while True:
                precision = toolbox.get_precision(predictions, truth, threshold)
                recall = toolbox.get_recall(predictions, truth, threshold)
                if first_pass:
                    first_pass = False
                    # this is to close the line on the y axis side, recall if precision would be 0
                    pr.append((0, recall, i, threshold))
                f1 = toolbox.get_f1(predictions, truth, threshold)
                if len(pr) == 0 or (
                    len(pr) > 0 and pr[-1][0:3] != (precision, recall, i)
                ):
                    pr.append((precision, recall, i, threshold))
                if config["calibration_fdr"] != "best_f1":
                    if (FDR_threshold == -1) and (
                        1 - precision <= config["calibration_fdr"]
                    ):
                        FDR_threshold = threshold
                elif f1 > best_f1:
                    best_f1 = f1
                    FDR_threshold = threshold

                if precision == 1:
                    # this is to close the line on the x axis side, precision if recall would be 0
                    pr.append((1, 0, i, threshold))
                    break
                if recall == 0:
                    break
                threshold += FILTERING_STEP

            thresholds.append(FDR_threshold)

        with open(output.filtering_threshold[0], "w") as outp:
            if config["calibration_function"] == "mean":
                outp.write(str(int(np.average(thresholds))))
            elif config["calibration_function"] == "median":
                outp.write(str(int(np.median(thresholds))))
            else:
                try:
                    outp.write(str(max(thresholds)))
                except ValueError:
                    outp.write('0')

        auprc = {}

        prev_x = 0
        prev_sample = None

        with open(output.prc[0], "w") as outp:
            for entry in pr:
                outp.write(
                    "{}\t{}\t{}\t{}\n".format(
                        input.predictions_file[entry[2]].split("/")[-1].split(".")[-3],
                        entry[0],
                        entry[1],
                        entry[3],
                    )
                )
                if (
                    prev_sample
                    == input.predictions_file[entry[2]].split("/")[-1].split(".")[-3]
                ):
                    x = entry[0] - prev_x
                else:
                    x = entry[0]
                prev_x = x
                prev_sample = (
                    input.predictions_file[entry[2]].split("/")[-1].split(".")[-3]
                )
                try:
                    auprc[
                        input.predictions_file[entry[2]].split("/")[-1].split(".")[-3]
                    ] += (x * entry[1])
                except KeyError:
                    auprc[
                        input.predictions_file[entry[2]].split("/")[-1].split(".")[-3]
                    ] = (x * entry[1])

        outp.close()

        with open(output.auprc[0], "w") as outp:
            for key in auprc:
                outp.write("{}\t{}\n".format(key, str(auprc[key])))


rule compute_precision:
    input:
        predictions_file=expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
        truth=expand(
            "{root}yaml/instances/samples/{{instance_name_nosuffix}}-{{evaluation_sample_ids}}.yaml",
            root=root,
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        filtering_threshold=expand(
            "{root}evaluations/filtering_threshold.{{toolname}}.{instance_name}.{{rank}}.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
    output:
        expand(
            "{root}evaluations/precision.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
    message:
        "Computing precision..."
    threads: 1
    run:
        with open(input.filtering_threshold[0]) as filtering_threshold_file:
            filtering_threshold = int(filtering_threshold_file.read())
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])

        truth = toolbox.load_truth(
            input.truth[0],
            reference_lists,
            config,
            wildcards.rank,
            input.unknown_lineages[0],
        )

        predictions = pd.read_csv(input.predictions_file[0], header=1, sep="\t")

        outp = open(output[0], "w")
        outp.write(str(toolbox.get_precision(predictions, truth, filtering_threshold)))
        outp.close()


rule compute_precision_threshold:
    input:
        predictions_file=expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
        truth=expand(
            "{root}yaml/instances/samples/{{instance_name_nosuffix}}-{{evaluation_sample_ids}}.yaml",
            root=root,
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
    output:
        expand(
            "{root}evaluations/precision_{{filtering_thresholds}}.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
            filtering_thresholds=str(config["filtering_threshold"]).split(".")[-1],
        ),
    message:
        "Computing precision for the fixed threshold..."
    threads: 1
    run:
        filtering_threshold = config["filtering_threshold"]
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])

        truth = toolbox.load_truth(
            input.truth[0],
            reference_lists,
            config,
            wildcards.rank,
            input.unknown_lineages[0],
        )

        predictions = pd.read_csv(input.predictions_file[0], header=1, sep="\t")

        outp = open(output[0], "w")
        outp.write(str(toolbox.get_precision(predictions, truth, filtering_threshold)))
        outp.close()


rule compute_tp:
    input:
        predictions_file=expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
        truth=expand(
            "{root}yaml/instances/samples/{{instance_name_nosuffix}}-{{evaluation_sample_ids}}.yaml",
            root=root,
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        filtering_threshold=expand(
            "{root}evaluations/filtering_threshold.{{toolname}}.{instance_name}.{{rank}}.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
    output:
        expand(
            "{root}evaluations/tp.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
    message:
        "Computing tp..."
    threads: 1
    run:
        with open(input.filtering_threshold[0]) as filtering_threshold_file:
            filtering_threshold = int(filtering_threshold_file.read())
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])

        truth = toolbox.load_truth(
            input.truth[0],
            reference_lists,
            config,
            wildcards.rank,
            input.unknown_lineages[0],
        )

        predictions = pd.read_csv(input.predictions_file[0], header=1, sep="\t")

        outp = open(output[0], "w")
        outp.write(str(toolbox.get_tp(predictions, truth, filtering_threshold)))
        outp.close()


rule compute_fp:
    input:
        predictions_file=expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
        truth=expand(
            "{root}yaml/instances/samples/{{instance_name_nosuffix}}-{{evaluation_sample_ids}}.yaml",
            root=root,
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        filtering_threshold=expand(
            "{root}evaluations/filtering_threshold.{{toolname}}.{instance_name}.{{rank}}.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
    output:
        expand(
            "{root}evaluations/fp.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
    message:
        "Computing fp..."
    threads: 1
    run:
        with open(input.filtering_threshold[0]) as filtering_threshold_file:
            filtering_threshold = int(filtering_threshold_file.read())
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])

        truth = toolbox.load_truth(
            input.truth[0],
            reference_lists,
            config,
            wildcards.rank,
            input.unknown_lineages[0],
        )

        predictions = pd.read_csv(input.predictions_file[0], header=1, sep="\t")

        outp = open(output[0], "w")
        outp.write(str(toolbox.get_fp(predictions, truth, filtering_threshold)))
        outp.close()


rule compute_f1_threshold:
    input:
        predictions_file=expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
        truth=expand(
            "{root}yaml/instances/samples/{{instance_name_nosuffix}}-{{evaluation_sample_ids}}.yaml",
            root=root,
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        filtering_threshold=expand(
            "{root}evaluations/filtering_threshold.{{toolname}}.{instance_name}.{{rank}}.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
    output:
        expand(
            "{root}evaluations/f1_{threshold}.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
            threshold=config["filtering_threshold"],
        ),
    message:
        "Computing f1 for the fixed threshold..."
    threads: 1
    run:
        filtering_threshold = config["filtering_threshold"]
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])

        truth = toolbox.load_truth(
            input.truth[0],
            reference_lists,
            config,
            wildcards.rank,
            input.unknown_lineages[0],
        )

        predictions = pd.read_csv(input.predictions_file[0], header=1, sep="\t")

        outp = open(output[0], "w")
        outp.write(str(toolbox.get_f1(predictions, truth, filtering_threshold)))
        outp.close()


rule compute_f1:
    input:
        predictions_file=expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
        truth=expand(
            "{root}yaml/instances/samples/{{instance_name_nosuffix}}-{{evaluation_sample_ids}}.yaml",
            root=root,
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        filtering_threshold=expand(
            "{root}evaluations/filtering_threshold.{{toolname}}.{instance_name}.{{rank}}.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
    output:
        expand(
            "{root}evaluations/f1.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
    message:
        "Computing f1..."
    threads: 1
    run:
        with open(input.filtering_threshold[0]) as filtering_threshold_file:
            filtering_threshold = int(filtering_threshold_file.read())
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])

        truth = toolbox.load_truth(
            input.truth[0],
            reference_lists,
            config,
            wildcards.rank,
            input.unknown_lineages[0],
        )

        predictions = pd.read_csv(input.predictions_file[0], header=1, sep="\t")

        outp = open(output[0], "w")
        outp.write(str(toolbox.get_f1(predictions, truth, filtering_threshold)))
        outp.close()


rule compute_l2:
    input:
        predictions_file=expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
        truth=expand(
            "{root}yaml/instances/samples/{{instance_name_nosuffix}}-{{evaluation_sample_ids}}.yaml",
            root=root,
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        filtering_threshold=expand(
            "{root}evaluations/filtering_threshold.{{toolname}}.{instance_name}.{{rank}}.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
    output:
        expand(
            "{root}evaluations/l2.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
    message:
        "Computing l2..."
    threads: 1
    run:
        with open(input.filtering_threshold[0]) as filtering_threshold_file:
            filtering_threshold = int(filtering_threshold_file.read())
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])

        truth = toolbox.load_truth(
            input.truth[0],
            reference_lists,
            config,
            wildcards.rank,
            input.unknown_lineages[0],
            for_l2=True
        )
        predictions = pd.read_csv(input.predictions_file[0], header=1, sep="\t")

        outp = open(output[0], "w")
        outp.write(str(toolbox.get_l2(predictions, truth, filtering_threshold)))
        outp.close()


rule precision_to_final_table:
    input:
        data=expand(
            "{root}evaluations/precision.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.precision.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.precision.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the precision for the fixed threshold table..."
    threads: 1
    run:
        dict_for_average = {}
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                sample = file.split("/")[-1].split(".")[2]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}\t{}\n".format(toolname, sample, rank, value))
                # keep the value for average
                try:
                    dict_for_average[toolname][rank].append(value)
                except KeyError:
                    try:
                        dict_for_average[toolname][rank] = [value]
                    except KeyError:
                        dict_for_average[toolname] = {}
                        dict_for_average[toolname][rank] = [value]
            for key_toolname in dict_for_average:
                for key_rank in dict_for_average[key_toolname]:
                    outp.write(
                        "{}\t{}\t{}\t{}\n".format(
                            key_toolname,
                            config["instance_name"],
                            key_rank,
                            np.average(
                                [
                                    float(v)
                                    for v in dict_for_average[key_toolname][key_rank]
                                ]
                            ),
                        )
                    )
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule precision_threshold_to_final_table:
    input:
        data=expand(
            "{root}evaluations/precision_{threshold}.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
            threshold=config["filtering_threshold"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.precision_{threshold}.tsv",
            instance_name=config["instance_name"],
            root=root,
            threshold=config["filtering_threshold"],
        ),
        expand(
            "{root}final_results/{instance_name}.precision_{threshold}.json",
            instance_name=config["instance_name"],
            root=root,
            threshold=config["filtering_threshold"],
        ),
    message:
        "Outputting the precision table..."
    threads: 1
    run:
        dict_for_average = {}
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                sample = file.split("/")[-1].split(".")[2]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}\t{}\n".format(toolname, sample, rank, value))
                # keep the value for average
                try:
                    dict_for_average[toolname][rank].append(value)
                except KeyError:
                    try:
                        dict_for_average[toolname][rank] = [value]
                    except KeyError:
                        dict_for_average[toolname] = {}
                        dict_for_average[toolname][rank] = [value]
            for key_toolname in dict_for_average:
                for key_rank in dict_for_average[key_toolname]:
                    outp.write(
                        "{}\t{}\t{}\t{}\n".format(
                            key_toolname,
                            config["instance_name"],
                            key_rank,
                            np.average(
                                [
                                    float(v)
                                    for v in dict_for_average[key_toolname][key_rank]
                                ]
                            ),
                        )
                    )
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule fp_to_final_table:
    input:
        data=expand(
            "{root}evaluations/fp.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.fp.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.fp.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the fp table..."
    threads: 1
    run:
        dict_for_average = {}
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                sample = file.split("/")[-1].split(".")[2]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}\t{}\n".format(toolname, sample, rank, value))
                # keep the value for average
                try:
                    dict_for_average[toolname][rank].append(value)
                except KeyError:
                    try:
                        dict_for_average[toolname][rank] = [value]
                    except KeyError:
                        dict_for_average[toolname] = {}
                        dict_for_average[toolname][rank] = [value]
            for key_toolname in dict_for_average:
                for key_rank in dict_for_average[key_toolname]:
                    outp.write(
                        "{}\t{}\t{}\t{}\n".format(
                            key_toolname,
                            config["instance_name"],
                            key_rank,
                            np.average(
                                [
                                    float(v)
                                    for v in dict_for_average[key_toolname][key_rank]
                                ]
                            ),
                        )
                    )
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule tp_to_final_table:
    input:
        data=expand(
            "{root}evaluations/tp.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.tp.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.tp.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the tp table..."
    threads: 1
    run:
        dict_for_average = {}
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                sample = file.split("/")[-1].split(".")[2]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}\t{}\n".format(toolname, sample, rank, value))
                # keep the value for average
                try:
                    dict_for_average[toolname][rank].append(value)
                except KeyError:
                    try:
                        dict_for_average[toolname][rank] = [value]
                    except KeyError:
                        dict_for_average[toolname] = {}
                        dict_for_average[toolname][rank] = [value]
            for key_toolname in dict_for_average:
                for key_rank in dict_for_average[key_toolname]:
                    outp.write(
                        "{}\t{}\t{}\t{}\n".format(
                            key_toolname,
                            config["instance_name"],
                            key_rank,
                            np.average(
                                [
                                    float(v)
                                    for v in dict_for_average[key_toolname][key_rank]
                                ]
                            ),
                        )
                    )
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule f1_to_final_table:
    input:
        data=expand(
            "{root}evaluations/f1.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.f1.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the f1 table..."
    threads: 1
    run:
        dict_for_average = {}
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                sample = file.split("/")[-1].split(".")[2]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}\t{}\n".format(toolname, sample, rank, value))
                # keep the value for average
                try:
                    dict_for_average[toolname][rank].append(value)
                except KeyError:
                    try:
                        dict_for_average[toolname][rank] = [value]
                    except KeyError:
                        dict_for_average[toolname] = {}
                        dict_for_average[toolname][rank] = [value]
            for key_toolname in dict_for_average:
                for key_rank in dict_for_average[key_toolname]:
                    outp.write(
                        "{}\t{}\t{}\t{}\n".format(
                            key_toolname,
                            config["instance_name"],
                            key_rank,
                            np.average(
                                [
                                    float(v)
                                    for v in dict_for_average[key_toolname][key_rank]
                                ]
                            ),
                        )
                    )
        toolbox.to_json(output[0], output[1], config=config, sort_with="self")


rule f1_threshold_to_final_table:
    input:
        data=expand(
            "{root}evaluations/f1_{threshold}.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            threshold=config["filtering_threshold"],
            rank=config["evaluation_taxlevel"],
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.f1_{threshold}.tsv",
            instance_name=config["instance_name"],
            root=root,
            threshold=config["filtering_threshold"],
        ),
        expand(
            "{root}final_results/{instance_name}.f1_{threshold}.json",
            instance_name=config["instance_name"],
            root=root,
            threshold=config["filtering_threshold"],
        ),
    message:
        "Outputting the f1 table..."
    threads: 1
    run:
        dict_for_average = {}
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                sample = file.split("/")[-1].split(".")[2]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}\t{}\n".format(toolname, sample, rank, value))
                # keep the value for average
                try:
                    dict_for_average[toolname][rank].append(value)
                except KeyError:
                    try:
                        dict_for_average[toolname][rank] = [value]
                    except KeyError:
                        dict_for_average[toolname] = {}
                        dict_for_average[toolname][rank] = [value]
            for key_toolname in dict_for_average:
                for key_rank in dict_for_average[key_toolname]:
                    outp.write(
                        "{}\t{}\t{}\t{}\n".format(
                            key_toolname,
                            config["instance_name"],
                            key_rank,
                            np.average(
                                [
                                    float(v)
                                    for v in dict_for_average[key_toolname][key_rank]
                                ]
                            ),
                        )
                    )
        toolbox.to_json(output[0], output[1], config=config, sort_with="self")


rule l2_to_final_table:
    input:
        data=expand(
            "{root}evaluations/l2.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.l2.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.l2.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the l2 table..."
    threads: 1
    run:
        sort_tool_by_rank = (
            lambda v: dict_for_ranking[rank][sample][v]
            if dict_for_ranking[rank][sample][v] != -1
            else 1 + max(list(dict_for_ranking[rank][sample].values()))
        )
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            dict_for_average = {}
            dict_for_ranking = {}
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                sample = file.split("/")[-1].split(".")[2]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    value = float(inp.read())
                outp.write("{}\t{}\t{}\t{}\n".format(toolname, sample, rank, value))

                # keep the value for average rank
                try:
                    dict_for_ranking[rank][sample].update({toolname: value})
                except KeyError:
                    try:
                        dict_for_ranking[rank].update({sample: {toolname: value}})
                    except KeyError:
                        dict_for_ranking.update({rank: {sample: {toolname: value}}})
            for rank in dict_for_ranking:
                for sample in dict_for_ranking[rank]:
                    ranking = list(dict_for_ranking[rank][sample].keys())
                    ranking.sort(key=sort_tool_by_rank, reverse=False)
                    for toolname in ranking:
                        try:
                            dict_for_average[toolname][rank].append(
                                ranking.index(toolname) + 1
                            )
                        except KeyError:
                            try:
                                dict_for_average[toolname][rank] = [
                                    ranking.index(toolname) + 1
                                ]
                            except KeyError:
                                dict_for_average[toolname] = {}
                                dict_for_average[toolname][rank] = [
                                    ranking.index(toolname) + 1
                                ]

            for key_toolname in dict_for_average:
                for key_rank in dict_for_average[key_toolname]:
                    outp.write(
                        "{}\t{}\t{}\t{}\n".format(
                            key_toolname,
                            config["instance_name"],
                            key_rank,
                            np.average(
                                [
                                    float(v)
                                    for v in dict_for_average[key_toolname][key_rank]
                                ]
                            ),
                        )
                    )

        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule compute_recall:
    input:
        predictions_file=expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
        truth=expand(
            "{root}yaml/instances/samples/{{instance_name_nosuffix}}-{{evaluation_sample_ids}}.yaml",
            root=root,
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        filtering_threshold=expand(
            "{root}evaluations/filtering_threshold.{{toolname}}.{instance_name}.{{rank}}.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
    output:
        expand(
            "{root}evaluations/recall.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
    message:
        "Computing recall..."
    threads: 1
    run:
        with open(input.filtering_threshold[0]) as filtering_threshold_file:
            filtering_threshold = int(filtering_threshold_file.read())
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])

        truth = toolbox.load_truth(
            input.truth[0],
            reference_lists,
            config,
            wildcards.rank,
            input.unknown_lineages[0],
        )

        predictions = pd.read_csv(input.predictions_file[0], header=1, sep="\t")

        outp = open(output[0], "w")
        outp.write(str(toolbox.get_recall(predictions, truth, filtering_threshold)))
        outp.close()


rule compute_recall_threshold:
    input:
        predictions_file=expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
        ),
        truth=expand(
            "{root}yaml/instances/samples/{{instance_name_nosuffix}}-{{evaluation_sample_ids}}.yaml",
            root=root,
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        filtering_threshold=expand(
            "{root}evaluations/filtering_threshold.{{toolname}}.{instance_name}.{{rank}}.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
    output:
        expand(
            "{root}evaluations/recall_{{threshold}}.{{toolname}}.{{instance_name_nosuffix}}{suffix}-{{evaluation_sample_ids}}.{{rank}}.tsv",
            root=root,
            suffix=suffix,
            filtering_thresholds=str(config["filtering_threshold"]).split(".")[-1],
        ),
    message:
        "Computing recall for the fixed threshold..."
    threads: 1
    run:
        filtering_threshold = config["filtering_threshold"]
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])

        truth = toolbox.load_truth(
            input.truth[0],
            reference_lists,
            config,
            wildcards.rank,
            input.unknown_lineages[0],
        )

        predictions = pd.read_csv(input.predictions_file[0], header=1, sep="\t")

        outp = open(output[0], "w")
        outp.write(str(toolbox.get_recall(predictions, truth, filtering_threshold)))
        outp.close()


rule recall_to_final_table:
    input:
        data=expand(
            "{root}evaluations/recall.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.recall.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.recall.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the recall for the fixed threshold table..."
    threads: 1
    run:
        dict_for_average = {}
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                sample = file.split("/")[-1].split(".")[2]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}\t{}\n".format(toolname, sample, rank, value))
                # keep the value for average
                try:
                    dict_for_average[toolname][rank].append(value)
                except KeyError:
                    try:
                        dict_for_average[toolname][rank] = [value]
                    except KeyError:
                        dict_for_average[toolname] = {}
                        dict_for_average[toolname][rank] = [value]
            for key_toolname in dict_for_average:
                for key_rank in dict_for_average[key_toolname]:
                    outp.write(
                        "{}\t{}\t{}\t{}\n".format(
                            key_toolname,
                            config["instance_name"],
                            key_rank,
                            np.average(
                                [
                                    float(v)
                                    for v in dict_for_average[key_toolname][key_rank]
                                ]
                            ),
                        )
                    )
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule recall_threshold_to_final_table:
    input:
        data=expand(
            "{root}evaluations/recall_{threshold}.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
            threshold=config["filtering_threshold"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.recall_{threshold}.tsv",
            instance_name=config["instance_name"],
            threshold=config["filtering_threshold"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.recall_{threshold}.json",
            instance_name=config["instance_name"],
            threshold=config["filtering_threshold"],
            root=root,
        ),
    message:
        "Outputting the recall table..."
    threads: 1
    run:
        dict_for_average = {}
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                sample = file.split("/")[-1].split(".")[2]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}\t{}\n".format(toolname, sample, rank, value))
                # keep the value for average
                try:
                    dict_for_average[toolname][rank].append(value)
                except KeyError:
                    try:
                        dict_for_average[toolname][rank] = [value]
                    except KeyError:
                        dict_for_average[toolname] = {}
                        dict_for_average[toolname][rank] = [value]
            for key_toolname in dict_for_average:
                for key_rank in dict_for_average[key_toolname]:
                    outp.write(
                        "{}\t{}\t{}\t{}\n".format(
                            key_toolname,
                            config["instance_name"],
                            key_rank,
                            np.average(
                                [
                                    float(v)
                                    for v in dict_for_average[key_toolname][key_rank]
                                ]
                            ),
                        )
                    )
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule memory_analysis_to_final_table:
    input:
        data=expand(
            "{root}analysis_outputs/{toolname}.{evaluation_sample}.memory_analysis.txt",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name_nosuffix"], s)
                for s in evaluation_samples_ids
            ],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.memory_analysis.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_analysis.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the memory for analysis table..."
    threads: 1
    run:
        dict_for_average = {}
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                sample = file.split("/")[-1].split(".")[1]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}".format(toolname, sample, value))
                # keep the value for average
                try:
                    dict_for_average[toolname].append(value)
                except KeyError:
                    dict_for_average[toolname] = [value]

            for key_toolname in dict_for_average:
                outp.write(
                    "{}\t{}\t{}\n".format(
                        key_toolname,
                        config["instance_name"],
                        np.average([float(v) for v in dict_for_average[key_toolname]]),
                    )
                )
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule runtime_analysis_to_final_table:
    input:
        data=expand(
            "{root}analysis_outputs/{toolname}.{evaluation_sample}.runtime_analysis.txt",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name_nosuffix"], s)
                for s in evaluation_samples_ids
            ],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.runtime_analysis.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_analysis.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the runtime for analysis table..."
    threads: 1
    run:
        dict_for_average = {}
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0\ntoolname\tsample\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                sample = file.split("/")[-1].split(".")[1]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}".format(toolname, sample, value))
                # keep the value for average
                try:
                    dict_for_average[toolname].append(value)
                except KeyError:
                    dict_for_average[toolname] = [value]

            for key_toolname in dict_for_average:
                outp.write(
                    "{}\t{}\t{}\n".format(
                        key_toolname,
                        config["instance_name"],
                        np.average([float(v) for v in dict_for_average[key_toolname]]),
                    )
                )
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule filtering_thresholds_to_final_table:
    input:
        data=expand(
            "{root}evaluations/filtering_threshold.{toolname}.{instance_name}.{rank}.tsv",
            toolname=toolnames,
            instance_name=config["instance_name"],
            rank=config["evaluation_taxlevel"],
            root=root,
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.filtering_threshold.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.filtering_threshold.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the filtering thresholds table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0|{}\ntoolname\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}\t{}\n".format(toolname, rank, value))
        toolbox.to_json(
            output[0], output[1], config=config, sort_with=input.sort[0], reverse=True
        )


rule auprc_to_final_table:
    input:
        data=expand(
            "{root}evaluations/auprc.{toolname}.{instance_name}.{rank}.tsv",
            toolname=toolnames,
            instance_name=config["instance_name"],
            rank=config["evaluation_taxlevel"],
            root=root,
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.auprc.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.auprc.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the auprc table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm|{}:v1.0\ntoolname\tsample\trank\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[1]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    for line in inp:
                        sample = line.strip().split("\t")[0]
                        value = line.strip().split("\t")[1]
                        outp.write(
                            "{}\t{}\t{}\t{}\n".format(toolname, sample, rank, value)
                        )
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule prc_to_final_table:
    input:
        data=expand(
            "{root}evaluations/prc.{toolname}.{instance_name}.{rank}.tsv",
            toolname=toolnames,
            instance_name=config["instance_name"],
            rank=config["evaluation_taxlevel"],
            root=root,
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.prc.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.prc.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the prc table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0|{}\ntoolname\trank\tsample\tprecision\trecall\tthreshold\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in sorted(input.data):
                toolname = file.split("/")[-1].split(".")[1]
                rank = file.split("/")[-1].split(".")[3]
                with open(file) as inp:
                    for line in inp:
                        value = line.strip().split("\t")
                        outp.write(
                            "{}\t{}\t{}\t{}\t{}\t{}\n".format(
                                toolname, rank, value[0], value[1], value[2], value[3]
                            )
                        )
                        # do not sort
        toolbox.to_json(output[0], output[1], config=config)


rule evaluated_predictions_by_tools_and_ranks:
    input:
        predictions=expand(
            "{root}evaluations/predictions.{{toolname}}.{{instance_name_nosuffix}}-{{all_sample_ids}}.{{rank}}.tsv",
            root=root,
        ),
        negative_samples=expand(
            "{root}analysis_outputs/{{toolname}}.{negative_sample}.predictions.tsv",
            root=root,
            negative_sample=[
                "{}-{}".format(config["instance_name_nosuffix"], s)
                for s in negative_samples_ids
            ],
        ),
        truth=expand(
            "{root}yaml/instances/samples/{instance_name}-{{all_sample_ids}}.yaml",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
        ref_genomes=expand(
            "{root}instances/{instance_name}/{instance_name}.{target}.all_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
            target=config["targets"],
        ),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name_nosuffix"],
        ),
        filtering_threshold=expand(
            "{root}evaluations/filtering_threshold.{toolname}.{instance_name}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            instance_name=config["instance_name_nosuffix"],
            rank=config["evaluation_taxlevel"],
        ),
    output:
        expand(
            "{root}final_results/{{toolname}}.{{instance_name_nosuffix}}-{{all_sample_ids}}.{{rank}}.tsv",
            root=root,
        ),
    message:
        "Producing the detailed evaluation of each sample for each tool at each rank"
    threads: 1
    run:
        reference_lists = []
        if "vir" in config["targets"]:
            reference_lists.append(input.vir_list[0])
        if "euk" in config["targets"]:
            reference_lists.append(input.euk_list[0])
        if "prok" in config["targets"]:
            reference_lists.append(input.prok_list[0])
        truth = toolbox.load_truth(
            input.truth[0],
            reference_lists,
            config,
            wildcards.rank,
            input.unknown_lineages[0],
        )

        # read the threshold
        for f in input.filtering_threshold:
            if f"filtering_threshold.{wildcards.toolname}.{wildcards.instance_name_nosuffix}.{wildcards.rank}.tsv" in f:
                break
        with open(f) as filtering_threshold_file:
            filtering_threshold = int(filtering_threshold_file.read())
        predictions = pd.read_csv(input.predictions[0], header=1, sep="\t")
        predictions = toolbox.evaluation_results(predictions, truth, filtering_threshold)

        # load the negative samples
        negative_samples_list = []
        for filename in input.negative_samples:
            df = pd.read_csv(filename, header=1, sep="\t")
            negative_samples_list.append(df)
        negative_samples = []
        if len(negative_samples_list) > 0:
            negative_samples = pd.concat(negative_samples_list, axis=0, ignore_index=True)
        if len(negative_samples) > 0 and len(predictions) > 0:
            negative_samples = toolbox.sum_to_taxlevel(
                negative_samples,
                input.ref_genomes,
                config["targets_taxonomy"],
                wildcards.rank,
                config,
            )

            predictions["in_negative_samples"] = predictions.apply(
                lambda row: row["evaluation_taxa"]
                in negative_samples["evaluation_taxa"].values,
                axis=1,
            )

        elif len(predictions) > 0:
            predictions["in_negative_samples"] = False

        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm:v1.0|{}\ntaxa\treads\tabundance\tcorrect_prediction\tappears_in_neg_samples\n".format(
                    config["targets_taxonomy"]
                )
            )
        predictions.to_csv(
            output[0],
            index=False,
            header=False,
            sep="\t",
            mode="a",
        )


rule evaluated_predictions_by_tools_and_ranks_to_json:
    input:
        expand(
            "{root}final_results/{toolname}.{{evaluation_sample}}.{rank}.tsv",
            toolname=toolnames,
            rank=config["evaluation_taxlevel"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{{evaluation_sample}}.json",
            root=root,
        ),
    message:
        "Producing the detailed evaluation of each evaluation sample for each tool at each rank in a single json"
    threads: 1
    run:
        col_names = [
            "toolname",
            "rank",
            "taxa",
            "reads",
            "abundance",
            "correct_prediction",
            "appears_in_neg_samples",
        ]
        all_data = pd.DataFrame(columns=col_names)
        for inp in input:
            data = pd.read_csv(inp, sep="\t", header=1)
            try:
                data.loc[:, "toolname"] = inp.split("/")[-1].split(".")[0]
                data.loc[:, "rank"] = inp.split("/")[-1].split(".")[-2]
            except ValueError:
                pass
            all_data = all_data.append(data, ignore_index=True)
            # drop 0 reads 0 abundance
        all_data = all_data[(all_data["reads"] != 0) | (all_data["abundance"] != 0.0)]
        try:
            all_data.sort_values(by=["toolname"], inplace=True)
        except KeyError:
            pass
        all_data.to_json(output[0], index=True, indent=4, orient="index")


rule memory_process_ref_host_to_final_table:
    input:
        data=expand(
            "{root}analysis_outputs/{toolname}.memory_process_ref_host.txt",
            root=root,
            toolname=toolnames,
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_host.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_host.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the memory for creating the host ref table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm|{}:v1.0\ntoolname\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}".format(toolname, value))
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule memory_process_ref_contaminants_to_final_table:
    input:
        data=expand(
            "{root}analysis_outputs/{toolname}.{instance_name}.memory_process_ref_contaminants.txt",
            root=root,
            toolname=toolnames,
            instance_name=config["instance_name_nosuffix"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_contaminants.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_contaminants.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the memory for creating the contaminants ref table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm|{}:v1.0\ntoolname\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}".format(toolname, value))
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule memory_process_ref_targets_to_final_table:
    input:
        data=expand(
            "{root}analysis_outputs/{toolname}.{instance_name}.memory_process_ref_targets.txt",
            root=root,
            toolname=toolnames,
            instance_name=config["instance_name_nosuffix"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_targets.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.memory_process_ref_targets.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the memory for creating the targets ref table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm|{}:v1.0\ntoolname\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}".format(toolname, value))
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule runtime_process_ref_host_to_final_table:
    input:
        data=expand(
            "{root}analysis_outputs/{toolname}.runtime_process_ref_host.txt",
            root=root,
            toolname=toolnames,
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_host.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_host.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the runtime for creating the host ref table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm|{}:v1.0\ntoolname\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}".format(toolname, value))
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule runtime_process_ref_contaminants_to_final_table:
    input:
        data=expand(
            "{root}analysis_outputs/{toolname}.{instance_name}.runtime_process_ref_contaminants.txt",
            root=root,
            toolname=toolnames,
            instance_name=config["instance_name_nosuffix"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_contaminants.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_contaminants.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the runtime for creating the contaminants ref table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm|{}:v1.0\ntoolname\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}".format(toolname, value))
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule runtime_process_ref_targets_to_final_table:
    input:
        data=expand(
            "{root}analysis_outputs/{toolname}.{instance_name}.runtime_process_ref_targets.txt",
            root=root,
            toolname=toolnames,
            instance_name=config["instance_name_nosuffix"],
        ),
        sort=expand(
            "{root}final_results/{instance_name}.f1.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_targets.tsv",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.runtime_process_ref_targets.json",
            instance_name=config["instance_name"],
            root=root,
        ),
    message:
        "Outputting the runtime for creating the targets ref table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write(
                "#LEMMI_dm|{}:v1.0\ntoolname\tvalue\n".format(
                    config["targets_taxonomy"]
                )
            )
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                with open(file) as inp:
                    value = inp.read()
                outp.write("{}\t{}".format(toolname, value))
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule create_frontend_additional_files:
    input:
        "{}../workflow/rules/textbox.json".format(root),
        "{}../workflow/rules/tool.json".format(root),
    output:
        expand(
            "{root}final_results/{instance_name}.structure.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.textbox.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.ranks.json",
            instance_name=config["instance_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{instance_name}.{toolname}.json",
            instance_name=config["instance_name"],
            root=root,
            toolname=toolnames,
        ),
    message:
        "Creating default json files for frontend specific content..."
    threads: 1
    run:
        # first to create structure.json (final concatenation made by the lemmi_analysis bash script)
        structure = (
            '\t"{}": {{\n\t\t"{}": [\n\t\t\t"textbox",'
            '\n\t\t\t"f1",'
            '\n\t\t\t"precision",'
            '\n\t\t\t"recall",'
            '\n\t\t\t"fp",'
            '\n\t\t\t"tp",'
            '\n\t\t\t"l2_rank",'
            '\n\t\t\t"memory",'
            '\n\t\t\t"runtime"'
            "\n\t\t]\n\t}}".format(config["instance_name"], config["instance_name"])
        )
        with open(output[0], "w") as outp:
            outp.write(structure)
            # a default textbox file for the instance
        shutil.copy(input[0], output[1])
        # the taxonomic ranks
        ranks = '{{\n\t"ranks": {}\n}}'.format(
            ["{}".format(t) for t in config["evaluation_taxlevel"]]
        )
        with open(output[2], "w") as outp:
            outp.write(ranks.replace("'", '"'))
            # for each tool, place a copy of the default tool.json
        for f in output[3:]:
            shutil.copy(input[1], f)


rule do_pairwise:
    message:
        "Producing pairwise distance between tools..."
    input:
        predictions=expand(
            "{root}evaluations/predictions.{toolname}.{evaluation_sample}.{rank}.tsv",
            root=root,
            toolname=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
    output:
        expand(
            "{root}evaluations/{{evaluation_sample}}.pairwise.{{toolnamesA}}.{{toolnamesB}}.{{rank}}.txt",
            root=root,
        ),
    params:
        dataset=config["instance_name_nosuffix"],
        root=root,
    run:
        # load the predictions toolnamesA as pandas dataframe
        dfA = pd.read_csv(
            f"{root}evaluations/predictions.{wildcards.toolnamesA}.{wildcards.evaluation_sample}.{wildcards.rank}.tsv",
            sep="\t",
            index_col=0,
            comment="#",
        )
        # load the predictions toolnamesB as pandas dataframe
        dfB = pd.read_csv(
            f"{root}evaluations/predictions.{wildcards.toolnamesB}.{wildcards.evaluation_sample}.{wildcards.rank}.tsv",
            sep="\t",
            index_col=0,
            comment="#",
        )
        merged_df = pd.merge(
            dfA, dfB, on="evaluation_taxa", how="outer", suffixes=("_df1", "_df2")
        )
        merged_df.fillna(0, inplace=True)
        # Extract abundance columns for calculations.
        abundance_df1 = merged_df["abundance_df1"].values
        abundance_df2 = merged_df["abundance_df2"].values
        # Calculate Bray-Curtis Dissimilarity
        bray_curtis_distance = distance.braycurtis(abundance_df1, abundance_df2)
        # write the value to the output file
        with open(output[0], "w") as outp:
            outp.write(str(bray_curtis_distance))


rule compute_pairwise:
    message:
        "fusioning pairwise"
    input:
        tools=expand(
            "{root}evaluations/{evaluation_sample}.pairwise.{toolnamesA}.{toolnamesB}.{rank}.txt",
            root=root,
            toolnamesA=toolnames,
            toolnamesB=toolnames,
            evaluation_sample=[
                "{}-{}".format(config["instance_name"],s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
        ),
    output:
        expand(
            "{root}final_results/{evaluation_sample}.pairwise.json",
            evaluation_sample=[
                "{}-{}".format(config["instance_name"], s)
                for s in evaluation_samples_ids
            ],
            rank=config["evaluation_taxlevel"],
            root=root,
        ),
    run:
        for o in output:
            o_with_path=o
            o=o.split("/")[-1]
            sample=o.split(".")[0]
            fields = {"A": [], "B": [], "Rank": [], "Distance": []}
            for f in input:
                if f.split("/")[-1].split(".")[-6] != sample:
                    continue
                rank = f.split(".")[-2]
                A = f.split(".")[-4]
                B = f.split(".")[-3]
                for line in open(f):
                    if not line.startswith("#"):
                        value = float(line.strip())
                fields["A"].append(A)
                fields["B"].append(B)
                fields["Rank"].append(rank)
                fields["Distance"].append(value)
            data = pd.DataFrame.from_dict(fields)
            try:
                data.sort_values(by=["A", "B"], inplace=True, ignore_index=True)
            except KeyError:
                pass
            data.to_json(o_with_path, index=True, indent=4, orient="index")


