Since the pipeline use tmp folder in order to allow the user to delete large reference after the pipeline has run,
but snakemake contain rules that depends on them to decide whether to rerun the pipeline or not
a function redirect the input of the rules to this existing/untouched file when all final and expected outputs are there
but the content of the tmp folder is missing, thus avoid a rerun of the pipeline
