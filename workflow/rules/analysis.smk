import glob
import os

import numpy as np
import pandas as pd
import toolbox

root = toolbox.set_root()
tmp = toolbox.set_tmp(root, config)

os.chdir(tmp)

toolname = config["container"].split("/")[-1].split(":")[0]
sif_name = config["container"].split("/")[-1]

container_runner, docker_container, clean = toolbox.define_docker_command(
    toolname,
    "container",
    root,
    tmp,
    config,
    int(workflow.cores / config["distribute_cores_to_n_tasks"])
    if workflow.cores > 1
    else 1,
    True,
)

if "real_samples" not in config:
    config["real_samples"] = []
if "pe" not in config:
    config["pe"] = 1
if "params_analysis" not in config:
    config["params_analysis"] = ""
# define what the samples are:
samples = toolbox.get_samples(root, config)

# define what the actual important output files are. They should be the same as the rule target inputs
# To be used in combination with the toolbox function define_tmp_folder_rules_inputs
# to control when to trigger a rerun if the tmp folder is missing.
# Useful as the user may want to delete a tmp folder to save the space of large reference db that are inputs of some rules
# To rerun, the user will have to remove at least one of these "real_target_files"

real_target_files = [
    expand(
        "{root}analysis_outputs/{toolname}.{instance_name}.memory_process_ref_targets.txt",
        root=root,
        toolname=toolname,
        instance_name=config["instance_name"],
    ),
    expand(
        "{root}analysis_outputs/{toolname}.{instance_name}.runtime_process_ref_targets.txt",
        root=root,
        toolname=toolname,
        instance_name=config["instance_name"],
    ),
    expand(
        "{root}analysis_outputs/{toolname}.memory_process_ref_host.txt",
        root=root,
        toolname=toolname,
    ),
    expand(
        "{root}analysis_outputs/{toolname}.runtime_process_ref_host.txt",
        root=root,
        toolname=toolname,
    ),
    expand(
        "{root}analysis_outputs/{toolname}.{instance_name}.memory_process_ref_contaminants.txt",
        root=root,
        toolname=toolname,
        instance_name=config["instance_name"],
    ),
    expand(
        "{root}analysis_outputs/{toolname}.{instance_name}.runtime_process_ref_contaminants.txt",
        root=root,
        toolname=toolname,
        instance_name=config["instance_name"],
    ),
    expand(
        "{root}analysis_outputs/{toolname}.{sample}.predictions.tsv",
        root=root,
        toolname=toolname,
        sample=samples,
    ),
    expand(
        "{root}analysis_outputs/{toolname}.{sample}.runtime_analysis.txt",
        root=root,
        toolname=toolname,
        sample=samples,
    ),
    expand(
        "{root}analysis_outputs/{toolname}.{sample}.memory_analysis.txt",
        root=root,
        toolname=toolname,
        sample=samples,
    ),
    expand(
        "{root}analysis_outputs/{toolname}.real_sample_{instance_name}-{real_sample}.predictions.tsv",
        root=root,
        toolname=toolname,
        real_sample=config["real_samples"],
        instance_name=config["instance_name"],
    ),
    expand(
        "{root}analysis_outputs/{toolname}.real_sample_{instance_name}-{real_sample}.runtime_analysis.txt",
        root=root,
        toolname=toolname,
        real_sample=config["real_samples"],
        instance_name=config["instance_name"],
    ),
    expand(
        "{root}analysis_outputs/{toolname}.real_sample_{instance_name}-{real_sample}.memory_analysis.txt",
        root=root,
        toolname=toolname,
        real_sample=config["real_samples"],
        instance_name=config["instance_name"],
    ),
]

toolbox.validate_config(config)


localrules:
    target,


rule target:
    input:
        memory_process_ref_targets=expand(
            "{root}analysis_outputs/{toolname}.{instance_name}.memory_process_ref_targets.txt",
            root=root,
            toolname=toolname,
            instance_name=config["instance_name"],
        ),
        runtime_process_ref_targets=expand(
            "{root}analysis_outputs/{toolname}.{instance_name}.runtime_process_ref_targets.txt",
            root=root,
            toolname=toolname,
            instance_name=config["instance_name"],
        ),
        memory_process_ref_host=expand(
            "{root}analysis_outputs/{toolname}.memory_process_ref_host.txt",
            root=root,
            toolname=toolname,
        ),
        runtime_process_ref_host=expand(
            "{root}analysis_outputs/{toolname}.runtime_process_ref_host.txt",
            root=root,
            toolname=toolname,
        ),
        memory_process_ref_contaminants=expand(
            "{root}analysis_outputs/{toolname}.{instance_name}.memory_process_ref_contaminants.txt",
            root=root,
            toolname=toolname,
            instance_name=config["instance_name"],
        ),
        runtime_process_ref_contaminants=expand(
            "{root}analysis_outputs/{toolname}.{instance_name}.runtime_process_ref_contaminants.txt",
            root=root,
            toolname=toolname,
            instance_name=config["instance_name"],
        ),
        predictions=expand(
            "{root}analysis_outputs/{toolname}.{sample}.predictions.tsv",
            root=root,
            toolname=toolname,
            sample=samples,
        ),
        runtime_analysis=expand(
            "{root}analysis_outputs/{toolname}.{sample}.runtime_analysis.txt",
            root=root,
            toolname=toolname,
            sample=samples,
        ),
        memory_analysis=expand(
            "{root}analysis_outputs/{toolname}.{sample}.memory_analysis.txt",
            root=root,
            toolname=toolname,
            sample=samples,
        ),
        read_predictions=expand(
            "{root}analysis_outputs/{toolname}.real_sample_{instance_name}-{real_sample}.predictions.tsv",
            root=root,
            toolname=toolname,
            real_sample=config["real_samples"],
            instance_name=config["instance_name"],
        ),
        real_runtime=expand(
            "{root}analysis_outputs/{toolname}.real_sample_{instance_name}-{real_sample}.runtime_analysis.txt",
            root=root,
            toolname=toolname,
            real_sample=config["real_samples"],
            instance_name=config["instance_name"],
        ),
        real_memory=expand(
            "{root}analysis_outputs/{toolname}.real_sample_{instance_name}-{real_sample}.memory_analysis.txt",
            root=root,
            toolname=toolname,
            real_sample=config["real_samples"],
            instance_name=config["instance_name"],
        ),


rule define_targets_and_contaminants:
    input:
        euk=expand(
            "{root}instances/{instance_name}/{instance_name}.euk.{ref_size}_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
            ref_size=config["ref_size"],
        ),
        prok=expand(
            "{root}instances/{instance_name}/{instance_name}.prok.{ref_size}_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
            ref_size=config["ref_size"],
        ),
        vir=expand(
            "{root}instances/{instance_name}/{instance_name}.vir.{ref_size}_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
            ref_size=config["ref_size"],
        ),
        host=expand("{root}repository/genomes/host.fna.gz", root=root),
    output:
        targets_ref=expand(
            "{tmp}targets_ref.{instance_name}.tsv",
            tmp=tmp,
            instance_name=config["instance_name"],
        ),
        contaminants_ref=expand(
            "{tmp}contaminants_ref.{instance_name}.tsv",
            tmp=tmp,
            instance_name=config["instance_name"],
        ),
    message:
        "Defining what is targets and contaminants..."
    threads: 1
    run:
        # define what the contaminants should be, if there is a host, and the target, and prepare corresponding files
        euk = pd.read_csv(
            input.euk[0],
            header=1,
            sep="\t",
        )
        euk["gtdb_taxonomy"] = np.nan
        euk["__gtdb_taxid"] = np.nan
        euk["__gtdb_accession"] = np.nan
        euk = euk.astype({"__gtdb_taxid": int}, errors="ignore")

        vir = pd.read_csv(
            input.vir[0],
            header=1,
            sep="\t",
        )
        vir["gtdb_taxonomy"] = np.nan
        vir["__gtdb_taxid"] = np.nan
        vir["__gtdb_accession"] = np.nan
        vir = vir.astype({"__gtdb_taxid": int}, errors="ignore")

        if config["use_prokncbi"] == 1:
            prok = pd.read_csv(
                input.prok[0],
                header=1,
                sep="\t",
            )
            prok["gtdb_taxonomy"] = np.nan
            prok["__gtdb_taxid"] = np.nan
            prok["__gtdb_accession"] = np.nan
            prok = prok.astype({"__gtdb_taxid": int}, errors="ignore")
        else:
            prok = pd.read_csv(
                input.prok[0],
                header=1,
                sep="\t",
            )
            prok["__size"] = prok["genome_size"]
            prok["taxid"] = prok["ncbi_taxid"]
            prok = prok.astype({"__gtdb_taxid": int}, errors="ignore")

            # consider the host as a regular genome for tool that do not have the concept of host
            # there is a symbolic link to the host genome in host folder from genomes/host.fna.gz,
            # thus the host will have "host" as accession
        host_size = toolbox.get_genome_size(input.host[0])
        series = pd.Series(
            ["host", config["host_taxid"], np.nan, np.nan, np.nan, np.nan, host_size],
            index=[
                "ncbi_genbank_assembly_accession",
                "taxid",
                "__lineage",
                "gtdb_taxonomy",
                "__gtdb_taxid",
                "__gtdb_accession",
                "__size",
            ],
        )
        host = pd.DataFrame([series])

        contaminants_ref = []

        if "prok" in config["contaminants"]:
            contaminants_ref = prok

        if "euk" in config["contaminants"]:
            if len(contaminants_ref) > 0:
                contaminants_ref = contaminants_ref.append(euk)
            else:
                contaminants_ref = euk

        if "vir" in config["contaminants"]:
            if len(contaminants_ref) > 0:
                contaminants_ref = contaminants_ref.append(vir)
            else:
                contaminants_ref = vir

        if "host" in config["contaminants"]:
            if len(contaminants_ref) > 0:
                contaminants_ref = contaminants_ref.append(host)
            else:
                contaminants_ref = host

        targets_ref = []

        if "prok" in config["targets"]:
            targets_ref = prok

        if "euk" in config["targets"]:
            if len(targets_ref) > 0:
                targets_ref = targets_ref.append(euk)
            else:
                targets_ref = euk

        if "vir" in config["targets"]:
            if len(targets_ref) > 0:
                targets_ref = targets_ref.append(vir)
            else:
                targets_ref = vir

        if "host" in config["targets"]:
            if len(targets_ref) > 0:
                targets_ref = targets_ref.append(host)
            else:
                targets_ref = host

        outp = open(output.contaminants_ref[0], "w")
        outp.write("# LEMMI content selected from NCBI+GTDB data\n")
        outp.close()
        try:
            contaminants_ref.to_csv(
                output.contaminants_ref[0],
                index=False,
                sep="\t",
                columns=[
                    "ncbi_genbank_assembly_accession",
                    "taxid",
                    "__lineage",
                    "gtdb_taxonomy",
                    "__gtdb_taxid",
                    "__gtdb_accession",
                    "__size",
                ],
                mode="a",
            )
        except AttributeError:
            pass

        outp = open(output.targets_ref[0], "w")
        outp.write("# LEMMI content selected from NCBI+GTDB data\n")
        outp.close()
        targets_ref.to_csv(
            output.targets_ref[0],
            index=False,
            sep="\t",
            columns=[
                "ncbi_genbank_assembly_accession",
                "taxid",
                "__lineage",
                "gtdb_taxonomy",
                "__gtdb_taxid",
                "__gtdb_accession",
                "__size",
            ],
            mode="a",
        )

if "targets_only" not in config or int(config["targets_only"]) != 1:
    rule process_ref_host:
        input:
            host_genome=expand(
                "{root}repository/host/{host_genome}",
                root=root,
                host_genome=config["host_genome"].split("/")[-1],
            ),
        output:
            memory_process_ref_host=expand(
                "{root}analysis_outputs/{toolname}.memory_process_ref_host.txt",
                root=root,
                toolname=toolname,
            ),
            runtime_process_ref_host=expand(
                "{root}analysis_outputs/{toolname}.runtime_process_ref_host.txt",
                root=root,
                toolname=toolname,
            ),
            ref_host=directory(
                expand(
                    "{tmp}ref_host",
                    tmp=tmp,
                )
            ),
        params:
            docker_suffix="host" if container_runner != "" else "",
            pe=config["pe"],
            other_params=config["other_params"],
        container:
            "{}sif/{}.sif".format(root, sif_name)
        message:
            "Processing reference for host..."
        threads: int(workflow.cores / config["distribute_cores_to_n_tasks"]) if workflow.cores > 1 else 1
        shell:
            """

            # run the tool
            {container_runner}{params.docker_suffix} {docker_container} {root}../workflow/scripts/task_wrapper.sh \
            LEMMI_process_ref_host.sh \
            {input.host_genome} {output.ref_host} {params.pe} {params.other_params} host

            mv {tmp}memory.host.txt {output.memory_process_ref_host}
            mv {tmp}runtime.host.txt {output.runtime_process_ref_host}

            # remove stopped docker containers
            {clean}

            """


    rule process_ref_contaminants:
        input:
            contaminants_ref=toolbox.define_tmp_folder_rules_inputs(
                expand(
                    "{tmp}contaminants_ref.{instance_name}.tsv",
                    tmp=tmp,
                    instance_name=config["instance_name"],
                ),
                "{}../workflow/rules/mock_input_file.txt".format(root),
                real_target_files,
            ),
        output:
            memory_process_ref_contaminants=expand(
                "{root}analysis_outputs/{toolname}.{instance_name}.memory_process_ref_contaminants.txt",
                root=root,
                toolname=toolname,
                instance_name=config["instance_name"],
            ),
            runtime_process_ref_contaminants=expand(
                "{root}analysis_outputs/{toolname}.{instance_name}.runtime_process_ref_contaminants.txt",
                root=root,
                toolname=toolname,
                instance_name=config["instance_name"],
            ),
            ref_contaminants=directory(
                expand(
                    "{tmp}ref_contaminants_{instance_name}",
                    tmp=tmp,
                    instance_name=config["instance_name"],
                )
            ),
        params:
            docker_suffix="conta" if container_runner != "" else "",
            contaminants_taxonomy=config["contaminants_taxonomy"],
            pe=config["pe"],
            other_params=config["other_params"],
        container:
            "{}sif/{}.sif".format(root, sif_name)
        message:
            "Processing reference for contaminants..."
        threads: int(workflow.cores / config["distribute_cores_to_n_tasks"]) if workflow.cores > 1 else 1
        shell:
            """

            # run the tool
            {container_runner}{params.docker_suffix} {docker_container} {root}../workflow/scripts/task_wrapper.sh \
            LEMMI_process_ref_contaminants.sh \
            {input.contaminants_ref} {output.ref_contaminants} {params.contaminants_taxonomy} {params.pe} {params.other_params} conta

            mv {tmp}memory.conta.txt {output.memory_process_ref_contaminants}
            mv {tmp}runtime.conta.txt {output.runtime_process_ref_contaminants}

            # remove stopped docker containers
            {clean}

            """
else:
    rule create_empty_files:
        output:
            memory_process_ref_contaminants=expand(
                "{root}analysis_outputs/{toolname}.{instance_name}.memory_process_ref_contaminants.txt",
                root=root,
                toolname=toolname,
                instance_name=config["instance_name"],
            ),
            runtime_process_ref_contaminants=expand(
                "{root}analysis_outputs/{toolname}.{instance_name}.runtime_process_ref_contaminants.txt",
                root=root,
                toolname=toolname,
                instance_name=config["instance_name"],
            ),
            ref_contaminants=directory(
                expand(
                    "{tmp}ref_contaminants_{instance_name}",
                    tmp=tmp,
                    instance_name=config["instance_name"],
                )
            ),
            memory_process_ref_host=expand(
                "{root}analysis_outputs/{toolname}.memory_process_ref_host.txt",
                root=root,
                toolname=toolname,
            ),
            runtime_process_ref_host=expand(
                "{root}analysis_outputs/{toolname}.runtime_process_ref_host.txt",
                root=root,
                toolname=toolname,
            ),
            ref_host=directory(
                expand(
                    "{tmp}ref_host",
                    tmp=tmp,
                )
            ),
        shell:
             """
             echo '0' > {output.memory_process_ref_contaminants}
             echo '0' > {output.memory_process_ref_host}
             echo '0' > {output.runtime_process_ref_contaminants}
             echo '0' > {output.runtime_process_ref_host}
             mkdir {output.ref_host}
             mkdir {output.ref_contaminants}
             """


rule process_ref_targets:
    input:
        targets_ref=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}targets_ref.{instance_name}.tsv",
                tmp=tmp,
                instance_name=config["instance_name"],
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
    output:
        memory_process_ref_targets=expand(
            "{root}analysis_outputs/{toolname}.{instance_name}.memory_process_ref_targets.txt",
            root=root,
            toolname=toolname,
            instance_name=config["instance_name"],
        ),
        runtime_process_ref_targets=expand(
            "{root}analysis_outputs/{toolname}.{instance_name}.runtime_process_ref_targets.txt",
            root=root,
            toolname=toolname,
            instance_name=config["instance_name"],
        ),
        ref_targets=directory(
            expand(
                "{tmp}ref_targets_{instance_name}",
                tmp=tmp,
                instance_name=config["instance_name"],
            )
        ),
    params:
        docker_suffix="target" if container_runner != "" else "",
        targets_taxonomy=config["targets_taxonomy"],
        pe=config["pe"],
        other_params=config["other_params"],
    container:
        "{}sif/{}.sif".format(root, sif_name)
    message:
        "Processing reference for targets..."
    threads: int(workflow.cores / config["distribute_cores_to_n_tasks"]) if workflow.cores > 1 else 1
    shell:
        """

        # run the tool
        {container_runner}{params.docker_suffix} {docker_container} {root}../workflow/scripts/task_wrapper.sh \
        LEMMI_process_ref_targets.sh \
        {input.targets_ref} {output.ref_targets} {params.targets_taxonomy} {params.pe} {params.other_params} target

        mv {tmp}memory.target.txt {output.memory_process_ref_targets}
        mv {tmp}runtime.target.txt {output.runtime_process_ref_targets}

        # remove stopped docker containers
        {clean}

        """


rule run_analysis:
    input:
        ref_targets=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}ref_targets_{instance_name}",
                tmp=tmp,
                instance_name=config["instance_name"],
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
        ref_host=toolbox.define_tmp_folder_rules_inputs(
            expand("{tmp}ref_host", tmp=tmp),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
        ref_contaminants=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}ref_contaminants_{instance_name}",
                tmp=tmp,
                instance_name=config["instance_name"],
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
        samples=expand(
            "{root}instances/{instance_name}/{{sample}}/",
            root=root,
            instance_name=config["instance_name"],
        ),
    output:
        predictions=expand(
            "{root}analysis_outputs/{toolname}.{{sample}}.predictions.tsv",
            root=root,
            toolname=toolname,
        ),
        runtime_analysis=expand(
            "{root}analysis_outputs/{toolname}.{{sample}}.runtime_analysis.txt",
            root=root,
            toolname=toolname,
        ),
        memory_analysis=expand(
            "{root}analysis_outputs/{toolname}.{{sample}}.memory_analysis.txt",
            root=root,
            toolname=toolname,
        ),
    wildcard_constraints:
        sample="(?!real_sample_).*",
    params:
        docker_suffix=(
            lambda wildcards: wildcards.sample if container_runner != "" else ""
        ),
        instance_name=config["instance_name"],
        contaminants_taxonomy=config["contaminants_taxonomy"],
        targets_taxonomy=config["targets_taxonomy"],
        pe=config["pe"],
        other_params=config["other_params"],
        script="LEMMI_targets_only.sh" if ("targets_only" in config and int(config["targets_only"]) == 1) else "LEMMI_analysis.sh"
    message:
        "run analysis..."
    container:
        "{}sif/{}.sif".format(root, sif_name)
    threads: int(workflow.cores / config["distribute_cores_to_n_tasks"]) if workflow.cores > 1 else 1
    shell:
        """

        # run the tool
        {container_runner}{params.docker_suffix} {docker_container} {root}../workflow/scripts/task_wrapper.sh \
        {params.script} \
        {wildcards.sample} {output.predictions} {params.instance_name} {params.contaminants_taxonomy} {params.targets_taxonomy} {params.pe} {params.other_params} analysis_{wildcards.sample}

        mv {tmp}memory.analysis_{wildcards.sample}.txt {output.memory_analysis}
        mv {tmp}runtime.analysis_{wildcards.sample}.txt {output.runtime_analysis}

        # remove stopped docker containers
        {clean}

        """
