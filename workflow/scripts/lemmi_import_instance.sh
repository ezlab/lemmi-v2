#!/bin/bash
shopt -s nullglob
if [[ -z "$LEMMI_ROOT" ]]; then
    echo "Must set LEMMI_ROOT in environment" 1>&2
    exit 1
fi

usage="Import an instance (i.e. config, samples, results, evaluations) from a different LEMMI install into the current one.\nUsage: lemmi_import_instances.sh path/to/the/root/of/another/lemmi/install/ instance_name"

if [ -z "$1" ]

then
    echo -e $usage && exit 1
fi

if [ -z "$2" ]
  then
    echo -e $usage && exit 1
fi
echo ""

shopt -u nullglob

ls $1 > /dev/null 2>&1 || { echo "ERROR: $1 does not exist"; exit $ERRCODE; }
ls $1/benchmark/instances/$2 > /dev/null 2>&1 || { echo "ERROR: $1/benchmark/instances/$2 does not exist"; exit $ERRCODE; }

echo -e "If absent, will import into LEMMI_ROOT (${LEMMI_ROOT}) the following elements in their corresponding locations:"
echo "$1/benchmark/yaml/instances/$2.yaml" in "$LEMMI_ROOT/benchmark/yaml/instances/$2.yaml"
echo "$1/benchmark/yaml/instances/samples/$2-*.yaml" in "$LEMMI_ROOT/benchmark/yaml/instances/samples/$2-*.yaml"
echo "$1/benchmark/yaml/runs/*.$2.yaml" in "$LEMMI_ROOT/benchmark/yaml/runs/*.$2.yaml"
echo "$1/benchmark/instances/$2" in "$LEMMI_ROOT/benchmark/instances/$2"
echo "$1/benchmark/analysis_outputs/*$2*" in "$LEMMI_ROOT/benchmark/analysis_outputs/*$2*"
echo "$1/benchmark/evaluations/*$2*" in "$LEMMI_ROOT/benchmark/evaluations/*$2*"
echo "$1/benchmark/final_results/*$2*" in "$LEMMI_ROOT/benchmark/final_results/*$2*"
echo ""
ls $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml 2> /dev/null && echo "ERROR: files matching $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml exist, first clean manually" && exit 1 || echo ""
ls $LEMMI_ROOT/benchmark/yaml/instances/samples/$2-*.yaml 2> /dev/null && echo "ERROR: files matching $LEMMI_ROOT/benchmark/yaml/instances/samples/$2-*.yaml exist, first clean manually" && exit 1 || echo ""
ls $LEMMI_ROOT/benchmark/yaml/runs/*.$2.yaml 2> /dev/null && echo "ERROR: files matching $LEMMI_ROOT/benchmark/yaml/runs/*.$2.yaml exist, first clean manually" && exit 1 || echo ""
ls $LEMMI_ROOT/benchmark/instances/$2 2> /dev/null && echo "ERROR: files exist in $LEMMI_ROOT/benchmark/instances/$2, first clean manually" && exit 1 || echo ""
ls $LEMMI_ROOT/benchmark/analysis_outputs/*$2* 2> /dev/null && echo "ERROR: files matching $LEMMI_ROOT/benchmark/analysis_outputs/*$2* exist, first clean manually" && exit 1 || echo ""
ls $LEMMI_ROOT/benchmark/evaluations/*$2* 2> /dev/null && echo "ERROR: files matching $LEMMI_ROOT/benchmark/evaluations/*$2* exist, first clean manually" && exit 1 || echo ""
ls $LEMMI_ROOT/benchmark/final_results/*$2* 2> /dev/null && echo "ERROR: files matching $LEMMI_ROOT/benchmark/final_results/*$2* exist, first clean manually" && exit 1 || echo ""

read -p "Are you sure? y/n" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    cp $1/benchmark/yaml/instances/$2.yaml $LEMMI_ROOT/benchmark/yaml/instances/
    cp $1/benchmark/yaml/instances/samples/$2-*.yaml $LEMMI_ROOT/benchmark/yaml/instances/samples/
    cp $1/benchmark/yaml/runs/*.$2.yaml $LEMMI_ROOT/benchmark/yaml/runs/
    cp -r $1/benchmark/instances/$2 $LEMMI_ROOT/benchmark/instances/$2
    cp $1/benchmark/analysis_outputs/*$2* $LEMMI_ROOT/benchmark/analysis_outputs/
    cp $1/benchmark/evaluations/*$2* $LEMMI_ROOT/benchmark/evaluations/
    cp $1/benchmark/final_results/*$2* $LEMMI_ROOT/benchmark/final_results/
    touch $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml
    touch -r $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml $LEMMI_ROOT/benchmark/yaml/instances/samples/$2-*.yaml
    touch -r $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml $LEMMI_ROOT/benchmark/yaml/runs/*.$2.yaml
    touch -r $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml $LEMMI_ROOT/benchmark/instances/$2
    touch -r $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml $LEMMI_ROOT/benchmark/instances/$2/*
    touch -r $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml $LEMMI_ROOT/benchmark/instances/$2/*/*
    touch -r $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml $LEMMI_ROOT/benchmark/analysis_outputs/*$2*
    touch -r $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml $LEMMI_ROOT/benchmark/evaluations/*$2*
    touch -r $LEMMI_ROOT/benchmark/yaml/instances/$2.yaml $LEMMI_ROOT/benchmark/final_results/*$2*
    echo "Done. Run LEMMI with the --dry-run option to evaluate how it affected your existing benchmarks (it should not have)"
fi

