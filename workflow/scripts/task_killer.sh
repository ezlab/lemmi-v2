#!/bin/bash
# this script will kill the task that is passed as parameters after 10 minutes
"$@" & # run the command in background
main_pid=$! # get the corresponding pid
i=0
while true;do
sleep 10
let "i+=10"
still_running="$(ps -o rss= $main_pid 2> /dev/null)" || break
if [[ "$i" -gt 600 ]]; then
  kill $main_pid
fi
done
