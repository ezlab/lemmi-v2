#!/bin/bash
shopt -s nullglob
if [[ -z "$LEMMI_ROOT" ]]; then
    echo "Must set LEMMI_ROOT in environment" 1>&2
    exit 1
fi

usage="Import a run (i.e. config, results, evaluations) from a different LEMMI install into the current one.\nUsage: lemmi_import_run.sh path/to/the/root/of/another/lemmi/install/ tool_name instance_name"

if [ -z "$1" ]

then
    echo -e $usage && exit 1
fi

if [ -z "$2" ]
  then
    echo -e $usage && exit 1
fi

if [ -z "$3" ]
  then
    echo -e $usage && exit 1
fi

run="$2.$3"

echo ""

shopt -u nullglob

ls $1 > /dev/null 2>&1 || { echo "ERROR: $1 does not exist"; exit $ERRCODE; }
ls $1/benchmark/yaml/runs/$run.yaml > /dev/null 2>&1 || { echo "ERROR: $1/benchmark/yaml/runs/$run does not exist"; exit $ERRCODE; }

echo -e "If absent, will import into LEMMI_ROOT (${LEMMI_ROOT}) the following elements in their corresponding locations:"
echo "$1/benchmark/yaml/runs/$run.yaml" in "$LEMMI_ROOT/benchmark/yaml/runs/$run.yaml"
echo "$1/benchmark/analysis_outputs/$run*" in "$LEMMI_ROOT/benchmark/analysis_outputs/$run*"
echo "$1/benchmark/evaluations/*$run*" in "$LEMMI_ROOT/benchmark/evaluations/*$run*"
echo ""
ls $LEMMI_ROOT/benchmark/yaml/runs/$run.yaml 2> /dev/null && echo "ERROR: files matching $LEMMI_ROOT/benchmark/yaml/runs/$run.yaml exist, first clean manually" && exit 1 || echo ""
ls $LEMMI_ROOT/benchmark/analysis_outputs/$run* 2> /dev/null && echo "ERROR: files matching $LEMMI_ROOT/benchmark/analysis_outputs/$run* exist, first clean manually" && exit 1 || echo ""
ls $LEMMI_ROOT/benchmark/evaluations/*$run* 2> /dev/null && echo "ERROR: files matching $LEMMI_ROOT/benchmark/evaluations/*$run* exist, first clean manually" && exit 1 || echo ""

read -p "Are you sure? y/n" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    cp $1/benchmark/yaml/runs/$run.yaml $LEMMI_ROOT/benchmark/yaml/runs/
    cp $1/benchmark/analysis_outputs/$run* $LEMMI_ROOT/benchmark/analysis_outputs/
    cp $1/benchmark/evaluations/*$run* $LEMMI_ROOT/benchmark/evaluations/
    touch $LEMMI_ROOT/benchmark/yaml/runs/$run.yaml
    touch -r $LEMMI_ROOT/benchmark/yaml/runs/$run.yaml $LEMMI_ROOT/benchmark/analysis_outputs/$run*
    touch -r $LEMMI_ROOT/benchmark/yaml/runs/$run.yaml $LEMMI_ROOT/benchmark/evaluations/*$run*

if [ !  -f $LEMMI_ROOT/benchmark/analysis_outputs/$2.memory_process_ref_host.txt ]; then
    cp $1/benchmark/analysis_outputs/$2.memory_process_ref_host.txt $LEMMI_ROOT/benchmark/analysis_outputs/$2.memory_process_ref_host.txt
    echo "$LEMMI_ROOT/benchmark/analysis_outputs/$2.memory_process_ref_host.txt added as well"
fi

if [ !  -f $LEMMI_ROOT/benchmark/analysis_outputs/$2.runtime_process_ref_host.txt ]; then
    cp $1/benchmark/analysis_outputs/$2.runtime_process_ref_host.txt $LEMMI_ROOT/benchmark/analysis_outputs/$2.runtime_process_ref_host.txt
    echo "$LEMMI_ROOT/benchmark/analysis_outputs/$2.runtime_process_ref_host.txt added as well"
fi

    echo "Done. Run LEMMI with the --dry-run option to evaluate how it affected your existing benchmarks. The files in final_results need to be recomputed."

fi

