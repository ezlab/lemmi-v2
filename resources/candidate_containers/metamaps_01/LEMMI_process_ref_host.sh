mkdir -p $2/taxonomy
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2/taxonomy
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
# get host taxid from config
taxid=$(cat ../../../config/config.yaml | grep host_taxid | grep -oh -P "[0-9].*")
gunzip -c $1 > host.fasta
perl /MetaMaps/util/addTaxonIDToFasta.pl --inputFA host.fasta --outputFA host.taxid.fasta --taxonID $taxid
perl /MetaMaps/buildDB.pl --DB $2/host --FASTAs host.taxid.fasta --taxonomy $2/taxonomy
