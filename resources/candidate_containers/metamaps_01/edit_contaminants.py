#!/usr/bin/env python3 
import sys, gzip
open('contaminants_input.fasta', 'w').close()
outp=open('contaminants_input.fasta', 'a')
i=0
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split()[0]
    for line in gzip.open("../../repository/genomes/{}.fna.gz".format(acc), 'rb'):
        line = line.decode('ascii')
        if line.startswith('>'):
            i+=1
            line = '>kraken:taxid|{}|C{}|{}\n'.format(taxid, i, acc)
        outp.write(line)
outp.close()
