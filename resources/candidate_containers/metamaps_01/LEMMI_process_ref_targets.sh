mkdir -p $2/taxonomy
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2/taxonomy
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
edit_targets.py $1 $3
perl /MetaMaps/buildDB.pl --DB $2/targets --FASTAs targets_input.fasta --taxonomy $2/taxonomy
