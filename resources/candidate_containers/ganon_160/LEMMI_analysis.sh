#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
ganon classify --db-prefix ../ref_host/ref_host --paired-reads ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz -o host --threads $cpus --output-all || true
filter_out_gz.py ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz host.all reads.r1.nohost.fastq
filter_out_gz.py ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz host.all reads.r2.nohost.fastq
ganon classify --db-prefix ../ref_contaminants_$3/ref_contaminants --paired-reads reads.r1.nohost.fastq reads.r2.nohost.fastq -o conta --threads $cpus --output-all || true
filter_out.py reads.r1.nohost.fastq conta.all reads.r1.noconta.fastq
filter_out.py reads.r2.nohost.fastq conta.all reads.r2.noconta.fastq
ganon classify --db-prefix ../ref_targets_$3/ref_targets --paired-reads reads.r1.noconta.fastq reads.r2.noconta.fastq -o targets --threads $cpus
ganon report --report-type reads -i targets.rep --output-prefix targets -f tsv --db-prefix ../ref_targets_$3/ref_targets --ranks superkingdom phylum class order family genus species
cat targets.tre | grep -v "root\|unclassified" | grep -v -P "^na\t" | cut -f7 > reads
cat targets.tre | grep -v "root\|unclassified" | grep -v -P "^na\t" | cut -f2 > taxids
cat targets.tre | grep -v "root\|unclassified" | grep -v -P "^na\t" | cut -f4 > organisms
echo -e "taxid\treads" > reads_abundance
paste taxids reads >> reads_abundance
abundance_reads_to_genomes_${taxo}.py reads_abundance ../ref_targets_$instance/references.tsv ../ref_targets_$instance/names.dmp abundance
paste organisms taxids reads abundance >> $output_file
