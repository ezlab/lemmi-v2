#!/usr/bin/env bash
set -o xtrace
set -e
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
mkdir -p $2
tar zxvf ../../repository/taxdump.tar.gz -C $2
# get host taxid from config
taxid=$(cat ../../../config/config.yaml | grep host_taxid | grep -oh -P "[0-9].*")
# get host size
size=$(gunzip -c $1 | grep -v ">" | wc -c)
echo -e "host.fna\thost\t$taxid" > seqinfo_host.txt
gunzip -c $1 > host.fna
sed -i 's/>.*/>host/' host.fna
ganon build-custom --ganon-path /ganon/build_cpp --db-prefix $2/ref_host \
            --input-file seqinfo_host.txt \
            --taxonomy-files $2/nodes.dmp $2/names.dmp \
            --threads $cpus

