#!/usr/bin/env bash
set -o xtrace
set -e
mkdir -p $2/taxonomy
mkdir -p $2/library
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2/taxonomy
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2/taxonomy
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
edit_contaminants.py $1 $3
/metacache/metacache build $2 metacache_contaminants_input.fasta -taxonomy $2/taxonomy -taxpostmap acc2taxid_contaminants.txt -kmerlen 32
