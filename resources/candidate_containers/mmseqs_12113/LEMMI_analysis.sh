#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
export MMSEQS_NUM_THREADS=$cpus
mmseqs createdb ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz queryDB_host
mmseqs taxonomy --search-type 3 queryDB_host ../ref_host/host hostResult host_tmp
mmseqs createtsv queryDB_host hostResult host.tsv
filter_out_gz.py ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz host.tsv reads.r1.nohost.fastq
mmseqs createdb reads.r1.nohost.fastq queryDB_conta
mmseqs taxonomy --search-type 3 queryDB_conta ../ref_contaminants_$instance/contaminants contaResult conta_tmp
mmseqs createtsv queryDB_conta contaResult conta.tsv
filter_out.py reads.r1.nohost.fastq conta.tsv reads.r1.noconta.fastq
mmseqs createdb reads.r1.noconta.fastq queryDB_targets
mmseqs taxonomy --search-type 3 queryDB_targets ../ref_targets_$instance/targets targetsResult targets_tmp
mmseqs taxonomyreport ../ref_targets_$instance/targets targetsResult report
echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
cat report | grep -v "unclassified\|root" | cut -f3 > reads
cat report | grep -v "unclassified\|root" | cut -f6 > organisms
cat report | grep -v "unclassified\|root" | cut -f5 > taxids
echo -e "taxid\treads" > reads_abundance
paste taxids reads | sed 's/ \{1,\}/ /g' >> reads_abundance
abundance_reads_to_genomes_${taxo}.py reads_abundance ../ref_targets_$instance/references.tsv ../ref_targets_$instance/names.dmp abundance
paste organisms taxids reads abundance | sed 's/ \{2,\}//g' >> $output_file
