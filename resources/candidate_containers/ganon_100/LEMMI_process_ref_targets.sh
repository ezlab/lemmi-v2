#!/usr/bin/env bash
set -o xtrace
set -e
mkdir -p $2
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
edit_targets.py $1 $3
ganon build --db-prefix $2/ref_targets \
            --input-files ganon_targets_input.fasta \
            --taxdump-file $2/nodes.dmp $2/names.dmp \
            --seq-info-file seqinfo_targets.txt --threads $cpus
cp $1  $2/references.tsv
