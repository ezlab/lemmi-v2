#!/usr/bin/env python3
#argv[1]: Fastq_host
#argv[2]: align out
#argv[3]: Fastq_noHost (to write)
import sys, gzip
read_id = set([])
outp = open(sys.argv[3], 'w')
"""
for line in open(sys.argv[2]): #FOR ALL THE CLASSIFIED
    if line.split('\t')[0] == 'C':
        read_id.add(line.split('\t')[1])
write = True
i=0
for line in gzip.open(sys.argv[1], 'r'):
    line = line.decode('ascii')
    if i % 4 == 0:
        if line.strip()[1:].split('/')[0] in read_id: 
            write = False
        else:
            write = True #FOR ALL THE UNCLASSIFIED

    if write:
        outp.write(line)
    i+=1
"""

#FOR BLAST, USING THE pident VALUE (70%)
for line in open(sys.argv[2]): #FOR ALL THE CLASSIFIED
    if line.split('\t')[5] > 70:
        read_id.add(line.split('\t')[0])
write = True
i=0
for line in gzip.open(sys.argv[1], 'r'):
    line = line.decode('ascii')
    if i % 4 == 0:
        if line.strip()[1:].split('/')[0] in read_id: 
            write = False
        else:
            write = True #FOR ALL THE UNCLASSIFIED

    if write:
        outp.write(line)
    i+=1

