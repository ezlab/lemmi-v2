#!/usr/bin/env python3
import sys, gzip
import os
import pandas as pd

targets_txt=open(sys.argv[1], 'r')
targets_out=open(sys.argv[2], 'r')
referes_tsv=open(sys.argv[3], 'r')
typeDB=sys.argv[4]
totalReads=int(sys.argv[5])
output_file=sys.argv[6]

#reading smash output.out file
#intersect_bp f_orig_query f_match f_unique_to_query f_unique_weighted average_abund median_abund std_abund name filename
#md5 f_match_orig unique_intersect_bp gather_result_rank remaining_bp query_filename query_name query_md5 query_bp
df = pd.read_csv(targets_out)
if df.empty:
    with open(output_file, 'w+') as file:
      file.write("#LEMMI_ncbi\n")
      file.write("organism\ttaxid\treads\tabundance\n")
      print("The CSV file is empty. Ending the program.")
      exit(0)  # Exit the program

selected_columns = ['f_unique_weighted', 'average_abund']
df = df[selected_columns]
df.columns = ['reads', 'abundance']
df['reads']=(df['reads']*totalReads)

#reading smash output.txt file
genome_acc=[]
#overlap p_query p_match avg_abund genomeName(GCA_001183745)
#Not matter the p_match value
for line in targets_txt:
  if line.startswith('overlap') or line.startswith('found') or line.startswith('-') or line.startswith('the '):
    continue
  elif len(line)>2:
    line=line.split('\n')[0]
    items=line.split("/")[-1]
    genome=items.split(".fna.gz")[0]
    genome_acc.append(genome)
df['genome_acc'] = genome_acc

#ncbi_genbank_assembly_accession taxid   __lineage       gtdb_taxonomy   __gtdb_taxid    __gtdb_accession        __size
columns = ['genome_acc', 'organism', 'taxid']
df2 = pd.DataFrame(columns=columns)
i=0
for line in referes_tsv:
  line=line.split('\n')[0]
  if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
    continue
  if typeDB == 'gtdb':
    taxid=line.split('\t')[4]
    organism=line.split('\t')[3]
    organism=organism.split(";")[-1]
  else:
    taxid=line.split('\t')[1]
    organism=line.split('\t')[2]
    organism=organism.split(";")[-1]
  acc=line.split()[0]
  size=line.split('\t')[6]

  if acc in genome_acc:
    df2.loc[i] = [acc,organism,taxid]
    i+=1

df = pd.merge(df, df2, on='genome_acc')
df=df[['organism', 'taxid','reads', 'abundance']]
df['reads'] = df['reads'].astype(int)
#print(df)
# normalize to 1
df['abundance'] = df['abundance'] / df['abundance'].sum()
#Output file
with open(output_file, 'w+') as file:
    file.write("#LEMMI_ncbi\n")
    file.write("organism\ttaxid\treads\tabundance\n")
    df.to_csv(file, header=False, sep='\t', index=False)
