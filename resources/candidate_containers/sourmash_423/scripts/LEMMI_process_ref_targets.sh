#!/usr/bin/env bash
set -o xtrace
set -e
smashdb=$2
edit_targets.py $1 $3
mkdir -p $smashdb/taxonomy
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $smashdb/taxonomy/
else
    tar zxvf ../../repository/taxdump.tar.gz -C $smashdb/taxonomy/
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
#targetsGenomes=`cat smash_targets_input_list.txt`
#sourmash sketch dna $targetsGenomes -o $smashdb/smash_targets_input.zip
targetsGenomes=smash_targets_input_list.txt
while read genome; do
    sourmash sketch dna $genome
done < $targetsGenomes
find . -name "*.sig" > sig.txt;
sourmash index --from-file sig.txt $smashdb/smash_targets_input.sbt.zip
cp $1  $smashdb/taxonomy/references.tsv
#Use sourmash gather to locate a query in the zip database

