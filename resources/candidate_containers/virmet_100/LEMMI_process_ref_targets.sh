#!/usr/bin/env bash
set -o xtrace
mkdir -p $2
mkdir -p viral_nuccore
# need to fake gi numbers (or retrieve real ones, but better faking to support potential sequences with no gi)
edit_targets.py $1 $3
edit_LEMMI_inputs.py viral_nuccore/viruses.fasta $1
virmet index --viral n
mv viral_nuccore $2
cp $1  $2/references.tsv
