#!/usr/bin/env bash
set -o xtrace
set -e
krakendb=$2
edit_host.py $1
mkdir -p $krakendb/taxonomy
tar zxvf ../../repository/taxdump.tar.gz -C $krakendb/taxonomy/
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
kraken2-build --threads $cpus --add-to-library kraken_host_input.fasta --db $krakendb
kraken2-build --threads $cpus --build --db $krakendb
