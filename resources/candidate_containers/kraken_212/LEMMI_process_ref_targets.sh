#!/usr/bin/env bash
set -o xtrace
set -e
krakendb=$2
edit_targets.py $1 $3
mkdir -p $krakendb/taxonomy
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $krakendb/taxonomy/
else
    tar zxvf ../../repository/taxdump.tar.gz -C $krakendb/taxonomy/
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
kraken2-build --threads $cpus --add-to-library kraken_targets_input.fasta --db $krakendb
kraken2-build --threads $cpus --build --db $krakendb
alias python=python3
/bracken2/bracken-build -d $krakendb -t $cpus -k 35 -l 150
cp $1  $krakendb/taxonomy/references.tsv
