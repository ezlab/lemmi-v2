#!/usr/bin/env bash
set -o xtrace
set -e
pe=$3
if [ $pe -eq 1 ]
then
params=""
else
params="-D 5"
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
mkdir $2
mkdir inp_host
gunzip -c $1 | grep -v "NNNNNNNNNNNNNNNNNNNNNN" > inp_host/host.fa
/kmcp compute -I inp_host -k 21 --split-number 10 --split-overlap 100 -O host_kmers -j $cpus --force $params
/kmcp index -j $cpus -I host_kmers -O $2 -n 1 -f 0.3
tar zxvf ../../repository/taxdump.tar.gz -C $2
echo -e "host\t9606" > $2/taxid.map
