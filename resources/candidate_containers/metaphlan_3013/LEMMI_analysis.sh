#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
pe=$7
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
rm bowtie_out || true
if [ $pe -eq 1 ]
then
metaphlan ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz,../../../instances/$instance/$sample_folder/reads.r2.fastq.gz --input_type fastq -o profiled_metagenome.txt --bowtie2out bowtie_out
else
gunzip -c ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz > reads.fastq
bowtie2 -p $cpus -q --local --no-head --no-sq --no-unal -S tmp.sam -x /root/miniconda3/lib/python3.7/site-packages/metaphlan/metaphlan_databases/mpa_v30_CHOCOPhlAn_201901 -U reads.fastq
metaphlan tmp.sam --input_type sam --nproc $cpus -o profiled_metagenome.txt --bowtie2out bowtie_out --index mpa_v30_CHOCOPhlAn_201901
fi
prepare_results.py profiled_metagenome.txt $output_file
