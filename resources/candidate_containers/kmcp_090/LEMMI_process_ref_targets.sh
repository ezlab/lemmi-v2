#!/usr/bin/env bash
set -o xtrace
set -e
pe=$4
if [ $pe -eq 1 ]
then
params=""
else
params="-D 5"
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
mkdir inp_target
python3 /usr/bin/prepare_targets.py $1 $3
for i in $(cut -f1 targets_taxid.map);do gunzip -c ../../repository/genomes/$i.fna.gz > inp_target/$i.fna;done
nb=$(cut -f3 $1 | grep -v "root;Viruses" | wc -l)
sn=10
if [ $nb == 2 ]; then sn=5;fi
/kmcp compute -I inp_target -k 21 --split-number $sn --split-overlap 100 -O target_kmers -j $cpus --force $params
/kmcp index -j $cpus -I target_kmers -O $2 -n 1 -f 0.3
mv targets_taxid.map $2/taxid.map
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2
fi
cp $1  $2/references.tsv
