#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
pe=$6
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
if [ $pe -eq 1 ]
then
/kmcp search -d ../ref_targets_$3 -1 ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz -2 ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz -o targets.tsv.gz
else
/kmcp search -d ../ref_targets_$3 -1 ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz -2 -o targets.tsv.gz
fi
/kmcp profile targets.tsv.gz \
    -m 1 \
    --taxid-map        ../ref_targets_$3/taxid.map \
    --taxdump          ../ref_targets_$3/ \
    --out-prefix       targets.tsv.gz.k.profile \
    --cami-report      targets.tsv.gz.c.profile \
    --binning-result   targets.tsv.gz.binning.gz
python3 /usr/bin/prepare_final_results.py targets.tsv.gz.k.profile $output_file $taxo
