../scripts/random_^Cim.py -i all.fq -o output_fasta -f fastq -l 150 -min 0 -max 75
docker run -w $(pwd) -v $(pwd):$(pwd) deepmicrobe /DeepMicrobes/pipelines/tfrec_train_kmer.sh -i output_fasta -v $(pwd)/tokens_merged_12mers.txt -o train.tfrec -s 20480000 -k 12
docker run -w $(pwd) -v $(pwd):$(pwd) deepmicrobe /DeepMicrobes/DeepMicrobes.py --input_tfrec=train.^Crec --model_name=attention --model_dir=results_train/ &
