#!/usr/bin/env python3
import sys, gzip
outp=open('list.txt','w').close()
outp=open('list.txt','a')
outp2=open('taxo.txt','w').close()
outp2=open('list.txt','a')
taxid=[]
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    acc=line.split('\t')[0]
    genome=acc+".fna"
    taxo=line.split('\t')[2].split(';')[-1]
    if line.split()[1] not in taxid:
        taxid.append(line.split()[1])
    outp.write(genome+'\t'+str(int(taxid.index(line.split()[1])))+'\n')
    outp2.write(genome+'\t'+str(int(taxid.index(line.split()[1])))+'\n')
outp.close()
