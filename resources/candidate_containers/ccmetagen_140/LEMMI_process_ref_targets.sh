#!/usr/bin/env bash
set -o xtrace
set -e
db=$2
edit_targets.py $1 $3
mkdir -p $db/targets
kma index -NI -Sparse T -i ccmetagen_targets_input.fasta -o $db/targets
if [ $3 = 'gtdb' ]; then
    python3 -c "from ete3 import NCBITaxa;NCBITaxa().update_taxonomy_database('../../repository/gtdb_taxdump.tar.gz')"
else
    python3 -c "from ete3 import NCBITaxa;NCBITaxa().update_taxonomy_database('../../repository/taxdump.tar.gz')"
fi
cp $1  $db/references.tsv
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2
fi
