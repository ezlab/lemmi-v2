#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
kma -ipe ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz -o host_out_kma -t_db ../ref_host/host -t $cpus -1t1 -mem_mode -and -apm f
filter_out_gz.py ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz host_out_kma.frag.gz reads.r1.nohost.fastq
filter_out_gz.py ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz host_out_kma.frag.gz reads.r2.nohost.fastq
#kma -ipe reads.r1.nohost.fastq reads.r2.nohost.fastq -o contaminants_out_kma -t_db ../ref_contaminants_$instance/conta -t $cpus -1t1 -mem_mode -and -apm f
#filter_out.py reads.r1.nohost.fastq contaminants_out_kma.frag.gz reads.r1.noconta.fastq
#filter_out.py reads.r2.nohost.fastq contaminants_out_kma.frag.gz reads.r2.noconta.fastq
cp reads.r1.nohost.fastq reads.r1.noconta.fastq
cp reads.r2.nohost.fastq reads.r2.noconta.fastq
kma -ef -ipe reads.r1.noconta.fastq reads.r2.noconta.fastq -o targets_out_kma -t_db ../ref_targets_$instance/targets -t $cpus -1t1 -mem_mode -and -apm f
CCMetagen.py -c 0 --query_identity 50 --depth 1 -i targets_out_kma.res -o results --depth_unit fr --mapstat targets_out_kma.mapstat
CCMetagen_merge.py -i .
echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
cat merged_samples.csv | grep -v "results.ccm.csv" | cut -f1 -d"," > reads
cat merged_samples.csv | grep -v "results.ccm.csv" | cut -f3,4,5,6,7,8,9 -d"," | tr -s "," | sed s/\,$//g | rev | cut -f1 -d"," | rev > organisms
echo -e "name\treads" > reads_abundance
paste organisms reads | sed 's/ \{1,\}/ /g' >> reads_abundance
abundance_reads_to_genomes_${taxo}.py reads_abundance ../ref_targets_$instance/references.tsv ../ref_targets_$instance/names.dmp abundance
paste organisms taxids reads abundance | sed 's/ \{2,\}//g' >> $output_file
