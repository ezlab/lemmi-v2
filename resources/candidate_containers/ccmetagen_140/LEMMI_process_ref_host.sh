#!/usr/bin/env bash
set -o xtrace
set -e
db=$2
edit_host.py $1
mkdir -p $db/host
kma index -NI -i ccmetagen_host_input.fasta -o $db/host
