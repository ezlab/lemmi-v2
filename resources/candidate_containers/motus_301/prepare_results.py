#!/usr/bin/env python3
import sys
outp=open(sys.argv[2], 'a')
for line in open(sys.argv[1]):
    if line.startswith('@') or line.startswith('#'):
        continue
    line = line.strip().split('\t')
    if line == [] or line == ['']:
        continue
    print(line)
    outp.write('{}\t{}\t{}\t{}\n'.format(line[3].split('|')[-1], line[0], 1000*float(line[4])/100, float(line[4])/100))
outp.close()
