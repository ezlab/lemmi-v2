#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
pe=$7
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
if [ $pe -eq 1 ]
then
python3 /mOTUs/motus profile -f ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz -r ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz -t $cpus -C precision > tmp_results
else
python3 /mOTUs/motus profile -s ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz -t $cpus -C precision > tmp_results
fi
prepare_results.py tmp_results $output_file
