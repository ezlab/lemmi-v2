#!/usr/bin/env python3 
import sys, gzip
open('metabuli_targets_accession2taxid.tsv', 'w').close()
outp2=open('metabuli_targets_accession2taxid.tsv', 'a')

i=0

for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    i+=1
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split()[0]
    outp=open("targets.{}.fna".format(acc),'w')
    for line in gzip.open("../../repository/genomes/{}.fna.gz".format(acc), 'rb'):
        line = line.decode('ascii')
        if line.startswith('>'):
            line = '>{}\n'.format(acc)
            line2 = '{}\t{}\t{}\t{}\n'.format(acc.split('.')[0], acc, taxid, i)
        outp.write(line)
        outp2.write(line2)
outp.close()
