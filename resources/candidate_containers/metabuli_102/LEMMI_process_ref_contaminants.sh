#!/usr/bin/env bash
set -o xtrace
set -e
db=$2
edit_contaminants.py $1 $3
mkdir -p $db/taxonomy
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $db/taxonomy/
    rm $db/taxonomy/merged.dmp
    touch $db/taxonomy/merged.dmp
    #echo -e "1\t|\t9999999999999\t|\n" > $db/taxonomy/merged.dmp
else
    tar zxvf ../../repository/taxdump.tar.gz -C $db/taxonomy/
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
ls contaminants.*.fna > list_contaminants.txt
/metabuli/bin/metabuli build $db list_contaminants.txt metabuli_contaminants_accession2taxid.tsv --threads $cpus --taxonomy-path $db/taxonomy/
cp $1  $db/taxonomy/references.tsv
