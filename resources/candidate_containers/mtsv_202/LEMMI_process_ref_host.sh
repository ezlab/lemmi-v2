#!/usr/bin/env bash
set -o xtrace
set -e
mtsv_idx=$2
edit_host.py $1
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
mkdir -p $mtsv_idx
index=`basename   $mtsv_idx`
ram=`grep MemTotal /proc/meminfo | awk '{print 0.05*$2/(1024*1024)}'`
echo "index_host: $index" >> config.yaml
echo "RAM_host: $ram" >> config.yaml
echo "cpus: $cpus" >> config.yaml
cp /app/Snakefile*.aba .
snakemake -p --cores 2 -s Snakefile1.aba
snakemake -p --cores 2 -s Snakefile2.aba
#sleep 20
