#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
cp  ../config.yaml  configAnalysis.yaml

if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
echo "index_join: ../../../instances/$instance/$sample_folder" >> configAnalysis.yaml
cp ../Snakefile0.aba .
snakemake --cores 2 -s Snakefile0.aba reads.rjoin.fasta
#joinReadsgz.py ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz \
#              ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz \
#              reads.rjoin.fasta

#Host
cp ../Snakefile3.aba .
snakemake --cores 2 -s Snakefile3.aba  mtsv_host_result.result
awk -F ":" '{print $1}' mtsv_host_result.result >  host.out
selectFasta -list host.out -fasta reads.rjoin.fasta  > reads.nohost.fasta || true

#CONT
cp ../Snakefile6.aba .
snakemake --cores 2 -s Snakefile6.aba  mtsv_cont_result.result
awk -F ":" '{print $1}' mtsv_cont_result.result >  conta.out
selectFasta -list conta.out -fasta reads.nohost.fasta  > reads.noconta.fasta || true

#target
cp ../Snakefile9.aba .
snakemake --cores 2 -s Snakefile9.aba  mtsv_targ_result.result
awk -F ":" '{print $1}' mtsv_targ_result.result >  targets.out
echo -e "taxid\treads" > reads_abundance
awk -F ":" '{print $2}' mtsv_targ_result.result | awk -F ","  '{print $1}' | sort -g  \
     | uniq -c | awk '{print $2"\t"$1}' >>  reads_abundance
abundance_reads_to_genomes_${taxo}.py reads_abundance ../targets_ref.$instance.tsv \
                          ../ref_targets_$instance/taxonomy/names.dmp abundance

#OUT
echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
cat abundance >> $output_file
#abundance_reads_to_genomes_gtdb.py targets.out ../targets_ref.demo_prok_gtdb.tsv \
#                          ../ref_targets_demo_prok_gtdb/taxonomy/names.dmp $output_file

