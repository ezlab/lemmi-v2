#!/usr/bin/env bash
set -o xtrace
set -e
cpus=$1
mtsv-binner -t $cpus \
   --fasta reads.rjoin.fasta \
   --index ../ref_host/mtsv_host_input_0.fasta.index \
   --results mtsv_host_input_0.fasta.index.result

mtsv-collapse mtsv_host_input_0.fasta.index.result \
    --output mtsv_host_result.result
