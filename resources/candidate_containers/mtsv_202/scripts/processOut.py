#!/usr/bin/env python3
import sys, gzip
import os

targets_txt=open(sys.argv[1], 'r')
targets_out=open(sys.argv[2], 'r')
referes_tsv=open(sys.argv[3], 'r')
typeDB=sys.argv[4]
totalReads=sys.argv[5]
outp       =open(sys.argv[6], 'w')


genomeList=set([])
genomeIdx=[]
organismDic={}
#overlap p_query p_match genomeName(GCA_001183745)
i=0
for line in targets_txt:
  if line.startswith('overlap') or line.startswith('found') or line.startswith('-') or line.startswith('the '):
    continue
  elif len(line)>2:
    line=line.split('\n')[0]
    items=line.split("/")[-1]
    genome=items.split(".fna.gz")[0]
    genomeList.add(genome)
    genomeIdx.append(genome)
    i+=1

#ncbi_genbank_assembly_accession taxid   __lineage       gtdb_taxonomy   __gtdb_taxid    __gtdb_accession        __size
for line in referes_tsv:
  line=line.split('\n')[0]
  if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
    continue
  if typeDB == 'gtdb':
    taxid=line.split('\t')[4]
    organism=line.split('\t')[3]
    organism=organism.split(";")[-1]
  else:
    taxid=line.split('\t')[1]
    organism=line.split('\t')[2]
    organism=organism.split(";")[-1]
  acc=line.split()[0]
  size=line.split('\t')[6]

  if acc in genomeList:
    aux=organism+"\t"+str(taxid)
    organismDic[acc]=aux


#intersect_bp f_orig_query f_match f_unique_to_query f_unique_weighted average_abund median_abund std_abund name filename
#md5 f_match_orig unique_intersect_bp gather_result_rank remaining_bp query_filename query_name query_md5 query_bp
i=0
for line in targets_out:
  if line.startswith('intersect_bp'):
    continue
  else:
    items=line.split(',')
    acc=genomeIdx[i]
    overlap=items[0]
    query=items[1]
    match=items[2]
    avg_abund=items[5]
    totalReads=float(query)*float(totalReads);
    aux=organismDic[acc]+"\t"+str(totalReads)+"\t"+query+"\n"
    organismDic[acc]=aux
    i+=1


#Output file
outp.write("#LEMMI_ncbi\n")
outp.write("organism\ttaxid\treads\tabundance\n") #reads organism or match
for key in organismDic.keys():
  line=organismDic[key]
  outp.write(line)
